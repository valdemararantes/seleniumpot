package br.com.galgo.testes.recursos_comuns.teste;

import br.com.galgo.testes.recursos_comuns.enumerador.config.Ambiente;
import br.com.galgo.testes.recursos_comuns.pageObject.TelaGalgo;
import br.com.galgo.testes.recursos_comuns.pageObject.TelaLogin;
import br.com.galgo.testes.recursos_comuns.utils.TesteUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import java.util.concurrent.CompletableFuture;

public abstract class Teste {

    private static final Logger log = LoggerFactory.getLogger(Teste.class);
    private static final String strLog = String.format(
        "\n************************************************************************************************\n{}\n"
            + "************************************************************************************************\n");
    public static CompletableFuture futureBrowserOpened;
    public static Ambiente ambiente;
    private final String PASTA_TESTE = "Log";
    protected TelaLogin telaLogin;
    private String nomeTeste;

    public static void inicializarBrowser() {
        futureBrowserOpened = CompletableFuture.runAsync(() -> TelaGalgo.abrirBrowser());
    }

    @BeforeMethod
    protected void setUp() {
        log.trace("SETUP inicial do teste. Define para Homologação caso ainda não esteja definido");
        ambiente = TesteUtils.configurarTeste(Ambiente.HOMOLOGACAO, PASTA_TESTE);
        Teste.inicializarBrowser();
    }

    @AfterMethod
    protected void tearDown() {
        TesteUtils.finalizarTesteSemImprimir(getNomeTeste());
    }

    public String getNomeTeste() {
        return nomeTeste;
    }

    public void setNomeTeste(String nomeTeste) {
        this.nomeTeste = nomeTeste;
        log.info(strLog, nomeTeste.replace(".png", ""));
    }
}

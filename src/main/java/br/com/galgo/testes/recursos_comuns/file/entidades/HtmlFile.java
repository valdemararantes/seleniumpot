package br.com.galgo.testes.recursos_comuns.file.entidades;

import br.com.galgo.testes.recursos_comuns.config.ConfiguracaoSistema;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by valdemar.arantes on 10/12/2015.
 */
public class HtmlFile {

    private static final Logger log = LoggerFactory.getLogger(HtmlFile.class);
    private static  int counter = 0;

    public static final String save(String html) {
        try {
            counter++;
            final String fileName = ConfiguracaoSistema.PATH_PROJETO + "/" + counter + ".html";
            FileWriter writer = new FileWriter(fileName);
            writer.write(html);
            return fileName;
        } catch (IOException e) {
            log.error(null, e);
            return "Erro ao gerar arquivo HTML: " + e.toString();
        }
    }
}

package br.com.galgo.testes.recursos_comuns.suite.listener;

import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.junit.runner.Description;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;
import org.junit.runner.notification.RunListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by valdemar.arantes on 06/04/2015.
 */
public class PerformanceListener extends RunListener {

    private static final Logger log = LoggerFactory.getLogger(PerformanceListener.class);
    private static Description runDescription;
    private Description testDescription;
    private DateTime startTime;

    /**
     * Called when an atomic test fails, or when a listener throws an exception.
     * <p/>
     * <p>In the case of a failure of an atomic test, this method will be called
     * with the same {@code Description} passed to
     * {@link #testStarted(Description)}, from the same thread that called
     * {@link #testStarted(Description)}.
     * <p/>
     * <p>In the case of a listener throwing an exception, this will be called with
     * a {@code Description} of {@link Description#TEST_MECHANISM}, and may be called
     * on an arbitrary thread.
     *
     * @param failure describes the test that failed and the exception that was thrown
     */
    @Override
    public void testFailure(Failure failure) throws Exception {
        super.testFailure(failure);
    }

    /**
     * Called when an atomic test has finished, whether the test succeeds or fails.
     *
     * @param description the runDescription of the test that just ran
     */
    @Override
    public void testFinished(Description description) throws Exception {
        DateTime endTime = new DateTime();
        Duration duration = new Duration(startTime, endTime);
        log.trace("startTime={}; endTime={}; execTime={}", startTime, endTime, duration);
        log.trace("{} concluído em {}", testDescription != null ? testDescription.getDisplayName() : "Description " +
                "nulo", duration);
        super.testFinished(description);
    }

    /**
     * Called when all tests have finished. This may be called on an
     * arbitrary thread.
     *
     * @param result the summary of the test run, including all the tests that failed
     */
    @Override
    public void testRunFinished(Result result) throws Exception {
        log.trace("{} concluído em {}", runDescription != null ? runDescription.getDisplayName() :
                "runDescription[NULL]", result.getRunTime());
        super.testRunFinished(result);
    }

    /**
     * Called before any tests have been run. This may be called on an
     * arbitrary thread.
     *
     * @param description describes the tests to be run
     */
    @Override
    public void testRunStarted(Description description) throws Exception {
        runDescription = description;
        String runDisplayName = description != null ? description.getDisplayName() : "Run Description nulo";
        log.trace("runDescription.getDisplayName()={}", runDisplayName);
        super.testRunStarted(description);
    }

    /**
     * Called when an atomic test is about to be started.
     *
     * @param description the runDescription of the test that is about to be run
     *                    (generally a class and method name)
     */
    @Override
    public void testStarted(Description description) throws Exception {
        testDescription = description;
        if (description == null) {
            log.trace("Description nulo");
        } else {
            log.trace("testDescription.getDisplayName()={}", description.getDisplayName());
        }
        startTime = new DateTime();
        super.testStarted(description);
    }
}

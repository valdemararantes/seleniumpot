package br.com.galgo.testes.recursos_comuns.pageObject;

import br.com.galgo.testes.recursos_comuns.config.ConfiguracaoSistema;
import br.com.galgo.testes.recursos_comuns.enumerador.config.TipoCampo;
import br.com.galgo.testes.recursos_comuns.exception.ErroAplicacao;
import br.com.galgo.testes.recursos_comuns.exception.ErroMenuIndisponivel;
import br.com.galgo.testes.recursos_comuns.file.entidades.HtmlFile;
import br.com.galgo.testes.recursos_comuns.teste.Teste;
import br.com.galgo.testes.recursos_comuns.utils.Config;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.Point;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

public class TelaGalgo {

    // TODO ecalderini: colocar em um arquivo de config.
    public static final int MAX_RETRY = 5;
    public static final int TIMEOUT_WAIT_ELEMENT = 10;
    private static final Logger log = LoggerFactory.getLogger(TelaGalgo.class);
    // TODO ecalderini: eu acredito que isto esta variavel estando publica ha
    // uma quebra de encapsulamento
    public static WebDriver driver;

    public static void abrirBrowser() {
        if (driver != null) {
            log.info("TelaGalgo.driver não nulo. Reutilizando o driver.");
            driver.manage().deleteAllCookies();
        } else {
            log.debug("Driver nulo. Instanciando");
            String configuredBrowser = Config.getString("app.navegador");
            if (StringUtils.isBlank(configuredBrowser)) {
                log.info("Nenhum navegador configurado. Utilizando o Firefox (default)");
                setFirefoxDriver();
            } else {
                log.info("Utilizando o navegador configurado {}", configuredBrowser);
                final Browser browser = Browser.valueOf(configuredBrowser);
                switch (browser) {
                    case firefox:
                        setFirefoxDriver();
                        break;
                    case phantomjs:
                        setPhantomJSDriver();
                        break;
                    case chrome:
                        setChromeDriver();
                        break;
                    default:
                        setFirefoxDriver();
                }
            }
            driver.manage().timeouts().pageLoadTimeout(3, TimeUnit.MINUTES);
            driver.manage().timeouts().setScriptTimeout(3, TimeUnit.MINUTES);
            driver.manage().deleteAllCookies();
            log.debug("Abrindo a URL {}", Teste.ambiente.getUrl());
            driver.get(Teste.ambiente.getUrl());
            log.debug("Esperando carregar");
            esperarTelaCarregar();
            log.debug("URL aberta. Retornando");
        }
    }

    /**
     * Abre uma URL. Aguarda por até 3 minutos pela carga da página.
     *
     * @param url
     */
    public static void abrirUrl(String url) {
        log.debug("Acessando a URL {}", url);
        Instant start = Instant.now();
        if (!Teste.futureBrowserOpened.isDone()) {
            try {
                Teste.futureBrowserOpened.get();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }

        if (driver == null) {
            log.error("driver do navegador nulo");
            throw new RuntimeException("Driver do navegador nulo");
        }
        driver.get(url);
        esperarTelaCarregar();

        log.trace("Thread do Browser liberada [{}]", Duration.between(start, Instant.now()));

        if (!TelaGalgo.verificaCampoVisivelPorId("nmUsuario")) {
            Assert.fail("Campo com id nmUsuario não foi encontrado na página de Login.");
        }
    }

    public static WebElement encontrarCampoPorClassName(String classNameCampo) {
        return encontrarCampo(By.className(classNameCampo));
    }

    public static WebElement encontrarCampoPorCss(String selector) {
        return encontrarCampo(By.cssSelector(selector));
    }

    public static WebElement encontrarCampoPorId(String idCampo) {
        return encontrarCampo(By.id(idCampo));
    }

    // Aguarda por até 3 minutos pela carga da página.

    public static WebElement encontrarCampoPorLink(String link) {
        return encontrarCampo(By.linkText(link));
    }

    public static WebElement encontrarCampoPorName(String name) {
        return encontrarCampo(By.name(name));
    }

    /**
     * Procura pelo xPath por 10 seg
     *
     * @param xPath XPath a ser encontrado
     * @return Elemento encontrado
     */
    public static WebElement encontrarCampoPorXPath(String xPath) {
        return encontrarCampo(By.xpath(xPath));
    }

    /**
     * Metodo que espera por N segungos, onde N é o paramero passado.
     */
    @Deprecated
    public static void esperarSegundos(int segundos) {
        try {
            Thread.sleep(segundos * 1000);
        } catch (final InterruptedException e) {
        }
    }

    /**
     * Metodo que utiliza-se de javascript para verificar se o evento de
     * readyState da pagina. Aguarda até que readyState tenha seu estatus
     * "complete".
     */
    public static void esperarTelaCarregar() {
        Instant start = Instant.now();
        Wait<WebDriver> wait = new WebDriverWait(driver, 180);
        wait.until(driver1 -> ((JavascriptExecutor) driver).executeScript("return document.readyState").equals(
                "complete"));
        Duration duration = Duration.between(start, Instant.now());
        if (duration.getSeconds() > 2 && log.isTraceEnabled()) {
            log.trace("Página carregada depois de {}", duration);
        }
    }

    /**
     * Este metodo recebe uma string e enum do tipo TelaGalgoEnum que pode ser
     * XPATH, ID, LINK
     *
     * @param idOrXPathOrLink a string equivalente a um XPATH, ID, LINK
     * @param enumType        um enum determinando XPATH, ID, LINK
     */
    public static void esperarTelaCarregar(String idOrXPathOrLink, TelaGalgoEnum enumType) {
        TelaGalgo.esperarTelaCarregar();

        //TODO ecalderini: fazer mais testes sem o laço.
        //quanto o laco foi tirado, o teste quebrou.
        //        for (int i = 0; i < MAX_RETRY; i++) {
        try {

            if (enumType == TelaGalgoEnum.XPATH) {
                WebDriverWait wait = new WebDriverWait(driver, TelaGalgo.TIMEOUT_WAIT_ELEMENT);
                wait.until(ExpectedConditions.elementToBeClickable(By.xpath(idOrXPathOrLink)));
                //encontrarCampoPorXPath(idOrXPathOrLink);

            } else if (enumType == TelaGalgoEnum.ID) {
                WebDriverWait wait = new WebDriverWait(driver, TelaGalgo.TIMEOUT_WAIT_ELEMENT);
                wait.until(ExpectedConditions.elementToBeClickable(By.id(idOrXPathOrLink)));
                //encontrarCampoPorId(idOrXPathOrLink);

            } else if (enumType == TelaGalgoEnum.LINK) {
                WebDriverWait wait = new WebDriverWait(driver, TelaGalgo.TIMEOUT_WAIT_ELEMENT);
                wait.until(ExpectedConditions.elementToBeClickable(By.linkText(idOrXPathOrLink)));
                //encontrarCampoPorLink(idOrXPathOrLink);
            }
        } catch (Exception e) {
            log.error("Erro ao procurar elemento {}.\nReferenciado por: {}\n", idOrXPathOrLink, enumType);
            log.error("HTML ATUAL DA PAGINA");
            //@formatter:off
            log.error("\n"
                            + "****************************************************************************************\n"
                            + "{}\n"
                            + "****************************************************************************************\n",
                    driver.getPageSource());
            //@formatter:on
            throw new RuntimeException(e);
        }
    }

    /**
     * Fecha o navegador.
     */
    public static void fecharBrowser() {
        if (driver != null) {
            try {
                driver.quit();
            } catch (Exception e) {
                log.error(null, e);
            }
            driver = null;
        }
        driver = null;
    }

    public static void garanteTextoNaTela(String texto) {
        String msgErro = "Campo com texto não encontrado [texto procurado=" + texto + "].";
        try {
            Assert.assertTrue(verificaTextoNaTela(texto), msgErro);
        } catch (Exception e) {
            Assert.fail(msgErro);
        }
    }

    public static void logout() {
        //esperarTelaCarregar();
        if (driver != null) {
            final WebElement logoutLink = driver.findElement(By.id("portalLogoutLink"));
            if (logoutLink != null) {
                logoutLink.click();
            } else {
                log.info("Link de logou (id = portalLogoutLink) não foi encontrado");
            }
            log.debug("Aguardando a URL conter \"sistemagalgo/wps/portal\"");
            new WebDriverWait(driver, TIMEOUT_WAIT_ELEMENT).until(ExpectedConditions.urlContains(
                    "sistemagalgo/wps/portal"));
        } else {
            log.info("driver nulo. Não há logout a se fazer.");
        }
    }

    public static void reiniciarBrowser(String url) {
        TelaGalgo.logout();
        abrirUrl(url);
    }

    public static void tirarFotoDaTela(WebElement element, String nomeArq) {
        if (element == null) {
            try {
                element = driver.findElement(By.cssSelector("body > div#FLYParent > table"));
            } catch (NoSuchElementException e) {
                element = driver.findElement(By.cssSelector("body"));
            }
        }

        log.debug("Tirando uma foto da tela");
        byte[] screenshotAsBytes = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
        log.debug("Definindo o pedaço da tela que deve ser salvo utilizando o elemento {}", element);
        Rectangle rectangle = new Rectangle(element.getLocation().getX(), element.getLocation().getY(),
                element.getSize().width, element.getSize().height);
        Point location = element.getLocation();
        BufferedImage bufferedImage;
        try {
            bufferedImage = ImageIO.read(new ByteArrayInputStream(screenshotAsBytes));

            if (rectangle.height <= 2000) {
                TirarFotoDeTelaPequena(nomeArq, rectangle, bufferedImage);
            } else {
                TirarFotoDeTelaGrande(nomeArq, rectangle, location, bufferedImage);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            log.trace("Fim do método");
        }
    }

    public static void tirarScreenShot(String caminho) {
        tirarFotoDaTela(null, caminho);
    }

    public static boolean verificaCampoVisivelPorClass(String classCampo) {
        try {
            return encontrarCampoPorClassName(classCampo).isDisplayed();
        } catch (Exception exception) {
            return false;
        }
    }

    public static boolean verificaCampoVisivelPorId(String idCampo) {
        try {
            return encontrarCampoPorId(idCampo).isDisplayed();
        } catch (Exception exception) {
            return false;
        }
    }

    public static void verificaCampoVisivelPorLink(String link, String nomeUsuario) throws ErroAplicacao {
        try {
            encontrarCampoPorLink(link);
        } catch (Exception e) {
            throw new ErroMenuIndisponivel(link, nomeUsuario);
        }
    }

    public static boolean verificaCampoVisivelPorName(String nameCampo) {
        try {
            return encontrarCampoPorClassName(nameCampo).isDisplayed();
        } catch (Exception exception) {
            return false;
        }
    }

    public static boolean verificaCampoVisivelPorXPath(String xPath, int timeout) {
        try {
            return encontrarCampoPorXPath(xPath).isDisplayed();
        } catch (Exception exception) {
            return false;
        }
    }

    public static boolean verificaCampoVisivelPorXPath(String xPath, int... timeouts) {
        int timeout = 10;
        if (timeouts.length > 0) {
            timeout = timeouts[0];
        }
        return verificaCampoVisivelPorXPath(xPath, timeout);
    }

    public static boolean verificaTextoNaTela(String texto) {
        boolean estaNaTela;
        try {
            estaNaTela = driver.getPageSource().contains(texto);
        } catch (Exception e) {
            estaNaTela = false;
        }
        return estaNaTela;
    }

    private static void TirarFotoDeTelaGrande(String nomeArq, Rectangle rectangle, Point location,
                                              BufferedImage bufferedImage) throws IOException {
        File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        int maxHeight = 400;

        BufferedImage destImage = bufferedImage.getSubimage(location.x, location.y, rectangle.width, maxHeight);
        BufferedImage destImageBottom = bufferedImage.getSubimage(location.x, rectangle.height - maxHeight,
                rectangle.width, maxHeight);

        ImageIO.write(destImage, "png", screenshot);

        File screenOutTop = instantiateFileCreatingPathIfNecessary(addSuffixToFileName(nomeArq, "_top"));
        log.debug("Salvando evidência {}", screenOutTop.getCanonicalPath());
        ImageIO.write(destImage, "png", screenOutTop);

        File screenOutBottom = instantiateFileCreatingPathIfNecessary(addSuffixToFileName(nomeArq, "_bottom"));
        log.debug("Salvando evidência {}", screenOutTop.getCanonicalPath());
        ImageIO.write(destImageBottom, "png", screenOutBottom);

        log.debug("Apagando o arquivo {}", screenshot.getCanonicalPath());

        if (FileUtils.deleteQuietly(screenshot)) {
            log.warn("Não foi possível apagar o arquivo {}", screenshot.getCanonicalPath());
        }

        showImages(destImage, destImageBottom);
    }

    private static void TirarFotoDeTelaPequena(String nomeArq, Rectangle rectangle, BufferedImage bufferedImage)
            throws IOException {
        BufferedImage destImage;
        try {
            destImage = bufferedImage.getSubimage(rectangle.x, rectangle.y, rectangle.width,
                    rectangle.height);
        } catch (Exception e) {
            log.warn("Erro ao recortar a imagem: {}", e.toString());
            destImage = bufferedImage;
        }
        log.debug("Salvando evidência {}", nomeArq);
        ImageIO.write(destImage, "png", instantiateFileCreatingPathIfNecessary(nomeArq));
        log.debug("Imagem salva com sucesso");
        showImages(destImage);
    }

    /**
     * Adiciona uma sufixo ao nome do arquivo.
     * <p>
     * Ex: filename = "C:\path\arq.txt", suffix = "_suf", retorna
     * "C:|path\arq_suf.txt"
     * </p>
     *
     * @param filename Nome do arquivo a receber o sufixo.
     * @param suffix   Sufixo que será acrescentado ao nome, antes da extensão.
     * @return Novo nome do arquivo, já com o sufixo adicionado.
     */
    private static String addSuffixToFileName(String filename, String suffix) {
        String fileBaseName = FilenameUtils.getBaseName(filename);
        String fileFolder = FilenameUtils.getFullPath(filename);
        String fileExt = FilenameUtils.getExtension(filename);
        return FilenameUtils.concat(fileFolder, fileBaseName + suffix + "." + fileExt);
    }

    private static WebElement encontrarCampo(By campo) {
        return driver.findElement(campo);
    }

    /**
     * Cria a estrutura de pastas de um arquivo file caso não exista.
     *
     * @param filename Arquivo cujia estrutura de pastas se deseja criar, caso não
     *                 exista
     */
    private static File instantiateFileCreatingPathIfNecessary(String filename) {
        log.debug("filename={}", filename);
        File file = new File(filename);
        try {
            log.debug("file={}", file.getCanonicalPath());
        } catch (IOException ignoreException) {
        }
        if (!file.getParentFile().exists()) {
            try {
                log.debug("Criando a estrutura de pastas {}", file.getParentFile().getCanonicalPath());
                if (!file.getParentFile().mkdirs()) {
                    throw new IOException("file.mkdirs() retornou falso");
                } else {
                    return file;
                }
            } catch (IOException e) {
                String errMsg = String.format("Estrutura da pasta para o arquivo %s não foi criada", filename);
                log.error(errMsg, e);
                throw new RuntimeException(errMsg);
            }
        } else {
            return file;
        }
    }

    private static void setFirefoxDriver() {
        log.debug("Iniciando o browser FirefoxDriver");

        final FirefoxProfile profile = new FirefoxProfile();
        final DesiredCapabilities capabilities = DesiredCapabilities.firefox();
        final String firefoxBinary = Config.getString("app.caminho_firefox_bin");
        if (StringUtils.isNotBlank(firefoxBinary)) {
            capabilities.setCapability(FirefoxDriver.BINARY, firefoxBinary);
        }
        if ("ANBSP-WK258".equals(System.getenv("COMPUTERNAME"))) {
            log.info("Carregando o firebug porque está rodando na máquina do Valdemar");
            String firebugPath = "C:\\Users\\valdemar.arantes\\AppData\\Roaming\\Mozilla\\Firefox\\Profiles"
                    + "\\kekedvfj.default\\extensions\\firebug@software.joehewitt.com.xpi";
            try {
                final File extensionToInstall = new File(firebugPath);
                if (extensionToInstall.exists()) {
                    profile.addExtension(extensionToInstall);
                }
            } catch (IOException e) {
                log.error(null, e);
            }
        }
        driver = null;
        System.gc(); // TODO ecalderini: apenas verificar se os erros de instabilidade param acontecer.
        capabilities.setCapability(FirefoxDriver.PROFILE, profile);
        try {
            driver = new FirefoxDriver(capabilities);
        } catch (Exception e) {
            log.error("Erro ao instanciar FirefoxDriver", e);
            throw new RuntimeException("Erro ao instanciar FirefoxDriver");
        }
    }

    private static void setPhantomJSDriver() {
        log.debug("Iniciando o browser PhantomJSDriver");
        DesiredCapabilities capabilities = DesiredCapabilities.phantomjs();
        final String binaryPath = Config.getString("app.caminho_phantomjs_bin");
        log.debug("Binario: {}", binaryPath);
        capabilities.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY, binaryPath);
        capabilities.setCapability("acceptSslCerts", true);
        capabilities.setCapability("trustAllSSLCertificates", true);
        ArrayList<String> cliArgsCap = new ArrayList<>();
        cliArgsCap.add("--web-security=no");
        cliArgsCap.add("--ssl-protocol=any");
        cliArgsCap.add("--ignore-ssl-errors=yes");
        cliArgsCap.add("--load-images=yes");
        cliArgsCap.add("--webdriver-loglevel=WARN");
        capabilities.setCapability(PhantomJSDriverService.PHANTOMJS_CLI_ARGS, cliArgsCap);
        try {
            driver = new PhantomJSDriver(capabilities);
        } catch (Exception e) {
            log.error("Erro ao instanciar PhantomJSDriver", e);
            throw new RuntimeException("Erro ao instanciar PhantomJSDriver");
        }
    }

    private static void setChromeDriver() {
        log.debug("Iniciando o browser Chrome...");
        System.setProperty("webdriver.chrome.driver", Config.getString("app.caminho.chrome"));
        try {
            driver = new ChromeDriver();
        } catch (Exception e) {
            log.error("Erro ao instanciar ChromeDriver", e);
            throw new RuntimeException("Erro ao instanciar ChromeDriver");
        }
    }

    private static void showFrame(JFrame frame) {
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        try {
            Thread.sleep(5000);
        } catch (InterruptedException ignored) {
        }
        frame.setVisible(false);
    }

    private static void showImages(Image img) {
        if ("ANBSP-WK258".equals(System.getenv("COMPUTERNAME"))) {
            log.debug("Mostrando a evidência na tela");

            JFrame editorFrame = new JFrame("Tela");
            editorFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

            ImageIcon imageIconTop = new ImageIcon(img);
            JLabel jLabelTop = new JLabel();
            jLabelTop.setIcon(imageIconTop);
            editorFrame.getContentPane().add(jLabelTop, BorderLayout.CENTER);

            showFrame(editorFrame);
        }

    }

    private static void showImages(Image imgTop, Image imgBottom) {
        if ("ANBSP-WK258".equals(System.getenv("COMPUTERNAME"))) {
            log.debug("Mostrando a evidência na tela");

            JFrame editorFrame = new JFrame("Tela - Partes Superior e Inferior");
            editorFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

            ImageIcon imageIconTop = new ImageIcon(imgTop);
            JLabel jLabelTop = new JLabel();
            jLabelTop.setIcon(imageIconTop);
            editorFrame.getContentPane().add(jLabelTop, BorderLayout.NORTH);

            ImageIcon imageIconBottom = new ImageIcon(imgBottom);
            JLabel jLabelBottom = new JLabel();
            jLabelBottom.setIcon(imageIconBottom);
            editorFrame.getContentPane().add(jLabelBottom, BorderLayout.SOUTH);

            showFrame(editorFrame);
        }
    }

    public void abrirSubMenu(String linkMenu, String linkSubMenu) {
        TelaGalgo.esperarTelaCarregar();

        if (verificaTextoNaTela(">" + linkSubMenu + "<")) {
            log.debug("Tentando clicar no submenu {}", linkSubMenu);
            clicarItemPorLink(linkSubMenu);
        } else {

            clicarMenu(linkMenu);
            TelaGalgo.esperarTelaCarregar();
            clicarItemPorLink(linkSubMenu);
            TelaGalgo.esperarTelaCarregar();//TODO ecalderini: nao eh necessario
        }
    }

    public void clicarBotaoConfirmar() {
        log.debug("Clicando no Botao Confirmar");

        TelaGalgo.esperarTelaCarregar(getXPathBotaoConfirmar(), TelaGalgoEnum.XPATH);
        clicarItemPorXPath(getXPathBotaoConfirmar());
        TelaGalgo.esperarTelaCarregar();//TODO ecalderini: nao eh necessario
    }

    public void clicarBotaoVoltar() {
        log.debug("Clicando no boltao voltar");

        TelaGalgo.esperarTelaCarregar("voltar", TelaGalgoEnum.ID);

        clicarItemPorId("voltar");
        TelaGalgo.esperarTelaCarregar();//TODO ecalderini: nao eh necessario
    }

    public void clicarItemPorClassName(String classNameCampo) {
        log.debug("Clicando item por Class Name: {}", classNameCampo);

        TelaGalgo.esperarTelaCarregar();
        encontrarCampoPorClassName(classNameCampo).click();
        TelaGalgo.esperarTelaCarregar();//TODO ecalderini: nao eh necessario
    }

    public void clicarItemPorCss(String selector) {
        log.debug("Clicando item por CSS: {}", selector);

        TelaGalgo.esperarTelaCarregar();
        encontrarCampoPorCss(selector).click();
        TelaGalgo.esperarTelaCarregar();//TODO ecalderini: nao eh necessario
    }

    public void clicarItemPorDom(String dom) {
        log.debug("Clicando no item de DOM {}", dom);

        TelaGalgo.esperarTelaCarregar();
        encontrarCampoPorDom(dom).click();
        TelaGalgo.esperarTelaCarregar();//TODO ecalderini: nao eh necessario
    }

    public void clicarItemPorId(String idCampo) {
        log.debug("Clicando no elemento de id {}", idCampo);

        TelaGalgo.esperarTelaCarregar();
        encontrarCampo(By.id(idCampo)).click();
        TelaGalgo.esperarTelaCarregar();//TODO ecalderini: nao eh necessario
    }

    public void clicarItemPorLink(String link) {
        log.debug("Clicando no link \"{}\"", link);

        encontrarCampoPorLink(link).click();
        TelaGalgo.esperarTelaCarregar();//TODO ecalderini: nao eh necessario
    }

    public void clicarItemPorName(String name) {
        log.debug("Clicando no item de nome {}", name);

        TelaGalgo.esperarTelaCarregar();
        encontrarCampoPorName(name).click();
        TelaGalgo.esperarTelaCarregar();//TODO ecalderini: nao eh necessario
    }

    public void clicarItemPorXPath(String XPath) {
        log.debug("Clicando no item de XPath \"{}\"", XPath);

        //TelaGalgo.esperarTelaCarregar(XPath, TelaGalgoEnum.XPATH);

        try {
            encontrarCampoPorXPath(XPath).click();
        } catch (Exception e) {
            String htmlFileName = HtmlFile.save(driver.getPageSource());
            log.error("Arquivo HTML: " + htmlFileName, e);
        }

        TelaGalgo.esperarTelaCarregar();// TODO ecalderini: aqui temos o
        // problema de nao conseguir esperar
        // carregar.
    }

    public void clicarMenu(String linkMenu) {
        log.debug("Clicando no item de linkMenu {}", linkMenu);

        TelaGalgo.esperarTelaCarregar();
        clicarItemPorLink(linkMenu);
        TelaGalgo.esperarTelaCarregar();//TODO ecalderini: nao eh necessario
    }

    public void clicarSelecionarTodos() {
        log.debug("Clicando em Selecionar Todos");

        TelaGalgo.esperarTelaCarregar();
        clicarItemPorXPath("/html/body/div[2]/table/tbody/tr[2]/td[2]/table/tbody/tr[3]/td/table/tbody/tr/td[2]/div"
                + "/table/tbody/tr/td/table/tbody/tr/td/div/form/table[6]/tbody/tr[2]/td/input[2]");
        TelaGalgo.esperarTelaCarregar();//TODO ecalderini: nao eh necessario
    }

    public boolean continuarTentando(int tentativas, String msgError) {
        tentativas++;
        return verificaTextoNaTela(msgError) && tentativas >= ConfiguracaoSistema.MAX_RETENTATIVA;
    }

    public void fecharAlertaBrowser() {
        Alert alert = driver.switchTo().alert();
        alert.accept();
    }

    public void garanteTextoNoCampoPorXPath(String texto, String XPath) {
        String msgErro = "Campo com texto não encontrado [texto procurado=" + texto + "].";
        try {
            Assert.assertTrue(verificaTextoNoCampoPorXPath(texto, XPath), msgErro);
        } catch (Exception e) {
            Assert.fail(msgErro);
        }
    }

    public String getMsgErro() {
        return "Ocorreu um erro no sistema. Por favor, entre em contato com o Suporte ao Usuário e informe a "
                + "ocorrêcia código 26.";
    }

    /**
     * Retorna o tempo para a carga da página utilizando a Navigation Timing API
     *
     * @return
     */
    public Duration getPageLoadTime() {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        long navigationStart = (Long) js.executeScript("return window.performance.timing.navigationStart;");
        long loadEventEnd = (Long) js.executeScript("return window.performance.timing.loadEventEnd;");
        log.debug("navigationStart={}, loadEventEnd={}", Instant.ofEpochMilli(navigationStart), Instant.ofEpochMilli(
                loadEventEnd));
        Duration loadTime = Duration.ofMillis(loadEventEnd - navigationStart);
        log.debug("Tempo de carga da página: {}", loadTime);
        return loadTime;
    }

    public String getTextoPorXPath(String XPath) {
        TelaGalgo.esperarTelaCarregar();
        String text = encontrarCampoPorXPath(XPath).getText();
        TelaGalgo.esperarTelaCarregar();//TODO ecalderini: nao eh necessario

        return text;
    }

    public String getValorCampoPorXPath(String xPath) {
        return encontrarCampoPorXPath(xPath).getAttribute("value");
    }

    public void limparCampoPorId(String idCampo) {
        encontrarCampo(By.id(idCampo)).clear();
    }

    public void limparCampoPorName(String nameCampo) {
        encontrarCampoPorClassName(nameCampo).clear();
    }

    public void preencheCampoPorId(String idCampo, String dado) {
        WebElement campo = encontrarCampoPorId(idCampo);
        preencheCampo(campo, dado);
    }

    public void preencheCampoPorName(String nameCampo, String dado) {
        WebElement campo = encontrarCampoPorName(nameCampo);
        preencheCampo(campo, dado);
    }

    public void preencheCampoXPath(String XPath, String dado) {
        WebElement campo = encontrarCampoPorXPath(XPath);
        preencheCampo(campo, dado);
    }

    public void preencherCampoSelect(String valorCampo, String nameSelect) {
        Select dropdown = new Select(driver.findElement(By.name(nameSelect)));
        dropdown.selectByValue(valorCampo);
    }

    public void preencherCampoSelect(String valorCampo) {
        Select dropdown = new Select(driver.findElement(By.tagName(TipoCampo.SELECT.getDescricao())));
        dropdown.selectByValue(valorCampo);
    }

    public void preencherCampoSelectById(String valorCampo, String id) {
        Select dropdown = new Select(driver.findElement(By.id(id)));
        dropdown.selectByValue(valorCampo);
    }

    public void preencherCampoSelectByXPath(String valorCampo, String XPath) {
        Select dropdown = new Select(driver.findElement(By.xpath(XPath)));
        dropdown.selectByValue(valorCampo);
    }

    public void removerCampoReadOnlyPorId(String idCampo) {
        removerCampoReadOnly(encontrarCampoPorId(idCampo));
    }

    public void removerCampoReadOnlyPorName(String nameCampo) {
        removerCampoReadOnly(encontrarCampoPorClassName(nameCampo));
    }

    public void removerCampoReadOnlyPorXPath(String XPath) {
        removerCampoReadOnly(encontrarCampoPorXPath(XPath));
    }

    public boolean temErroNaTela() {
        try {
            return verificaTextoNaTela(getMsgErro()) || verificaTextoNaTela("encontra-se com status Suspenso.");
        } catch (Exception e) {
            return false;
        }
    }

    public boolean verificaCampoHabilitadoPorId(String idCampo) {
        try {
            return encontrarCampoPorId(idCampo).isEnabled();
        } catch (Exception exception) {
            return false;
        }
    }

    public boolean verificaCampoHabilitadoPorName(String nameCampo) {
        try {
            return encontrarCampoPorClassName(nameCampo).isEnabled();
        } catch (Exception exception) {
            return false;
        }
    }

    public boolean verificaCampoHabilitadoPorXPath(String xpath) {
        try {
            return encontrarCampoPorXPath(xpath).isEnabled();
        } catch (Exception exception) {
            return false;
        }
    }

    public boolean verificaCampoSelecionalPorName(String nameCampo) {
        try {
            return encontrarCampoPorClassName(nameCampo).isSelected();
        } catch (Exception exception) {
            return false;
        }
    }

    public boolean verificaCampoSelecionavelPorId(String idCampo) {
        try {
            return encontrarCampoPorId(idCampo).isSelected();
        } catch (Exception exception) {
            return false;
        }
    }

    public boolean verificaTextoNoCampoPorXPath(String texto, String XPath) {
        String textoNaTela = encontrarCampoPorXPath(XPath).getText();
        if (!textoNaTela.equals(texto)) {
            return false;
        }
        return true;
    }

    public void verificarItemEncontrado() {
        boolean estaNaTela = verificaTextoNaTela("1 item encontrado.") || verificaTextoNaTela("itens encontrados");
        Assert.assertTrue(estaNaTela, "Busca não trouxe nenhum resultado.");
    }

    /**
     * Salva em arquivo um printscreen da tela. O arquivo é salvo em ./log/<nm>_<timestamp>
     *
     * @param nm
     */
    protected void gerarEvidenciasDeErroParaLog(String nm) {
        LocalDateTime now = LocalDateTime.now();
        String nowStr = now.toString().replaceAll(":", ".");
        try {
            tirarScreenShot(String.format("./log/%s_%s.png", nm, nowStr));
            Files.write(Paths.get(String.format("./log/%s_%s.html", nm, nowStr)), driver.getPageSource().getBytes());
        } catch (IOException e) {
            log.error(null, e);
        }
    }

    private WebElement encontrarCampoPorDom(String dom) {
        if (StringUtils.isBlank(dom)) {
            return null;
        }
        if (driver instanceof JavascriptExecutor) {
            JavascriptExecutor js = (JavascriptExecutor) driver;
            return (WebElement) js.executeScript("return " + dom);
        } else {
            throw new RuntimeException("O driver do navegador não é um JavascriptExecutor");
        }
    }

    private String getXPathBotaoConfirmar() {
        return "//input[@value='Confirmar']";
    }

    private void preencheCampo(WebElement campo, String dado) {
        ((JavascriptExecutor) driver).executeScript("arguments[0].setAttribute('value', arguments[1]);", campo, dado);
    }

    private void removerCampoReadOnly(WebElement campo) {
        if (driver instanceof JavascriptExecutor) {
            JavascriptExecutor js = (JavascriptExecutor) driver;
            js.executeScript("arguments[0].removeAttribute('readonly')", campo);
        } else {
            log.warn("O driver do browser não é um JavascriptExecutor. Não foi possível remover o readonly do campo {}",
                    campo);
        }

    }

    private static enum Browser {firefox, phantomjs, chrome}

    public enum TelaGalgoEnum {
        XPATH, ID, LINK
    }
}

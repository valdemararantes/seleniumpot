package br.com.galgo.testes.recursos_comuns.teste;

import br.com.galgo.testes.recursos_comuns.enumerador.config.Ambiente;
import br.com.galgo.testes.recursos_comuns.enumerador.menu.Menu;
import br.com.galgo.testes.recursos_comuns.enumerador.menu.SubMenu;
import br.com.galgo.testes.recursos_comuns.enumerador.usuario.Categoria;
import br.com.galgo.testes.recursos_comuns.enumerador.usuario.Papel;
import br.com.galgo.testes.recursos_comuns.enumerador.usuario.UsuarioConfig;
import br.com.galgo.testes.recursos_comuns.exception.ErroAplicacao;
import br.com.galgo.testes.recursos_comuns.file.entidades.Usuario;
import br.com.galgo.testes.recursos_comuns.pageObject.TelaGalgo;
import br.com.galgo.testes.recursos_comuns.pageObject.TelaHome;
import br.com.galgo.testes.recursos_comuns.pageObject.TelaLogin;
import br.com.galgo.testes.recursos_comuns.pageObject.TelaGalgo.TelaGalgoEnum;
import br.com.galgo.testes.recursos_comuns.pageObject.compromisso.TelaPainelCompromissos;
import br.com.galgo.testes.recursos_comuns.utils.TesteUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class TesteConsultaCompromisso extends Teste {

    private static final Logger log = LoggerFactory.getLogger(TesteConsultaCompromisso.class);

    private final String PASTA_TESTE = "Compromisso";
    private TelaLogin telaLogin;
    private Ambiente ambiente;
    private UsuarioConfig usuarioConfig;

    public void configura(Ambiente ambiente, UsuarioConfig usuarioConfig) {
        this.ambiente = ambiente;
        this.usuarioConfig = usuarioConfig;
    }

    @BeforeMethod
    public void setUp() {
        ambiente = TesteUtils.configurarTeste(Ambiente.HOMOLOGACAO, PASTA_TESTE);

        if (TelaGalgo.driver == null) {
            Teste.inicializarBrowser();
        }

        telaLogin = TelaLogin.irParaTelaLogin();

        if (usuarioConfig == null) {
            usuarioConfig = UsuarioConfig.fromCategoria(ambiente, Categoria.USUARIO_FINAL, Papel.ADM_SISTEMA);
        }
    }

    @AfterMethod
    public void tearDown() {
        TesteUtils.finalizarTeste(this.getNomeTeste());
    }

    @Test
    public void testeConsultaPainelCompromissos() throws ErroAplicacao {
        this.setNomeTeste("testeConsultaPainelCompromissos.png");

        // TODO remover
        // int tentativas = ConfiguracaoSistema.MAX_RETENTATIVA;
        // Fixando em apenas uma tentativa
        int tentativas = 1;

        boolean abriuPainel = testePainelCompromisso();

        while (!abriuPainel && haTentativas(tentativas)) {
            tentativas--;
            TelaGalgo.reiniciarBrowser(ambiente.getUrl());
            abriuPainel = testePainelCompromisso();
        }

        Assert.assertTrue(abriuPainel, "Erro ao acessar o painel de compromissos!");
    }

    private boolean haTentativas(int tentativas) {
        return tentativas > 0;
    }

    private boolean testePainelCompromisso() throws ErroAplicacao {

        Usuario usuario = new Usuario(usuarioConfig);

        TelaHome telaHome = telaLogin.loginAs(usuario);
        
        TelaGalgo.esperarTelaCarregar(Menu.COMPROMISSOS.getLink(), TelaGalgoEnum.LINK);
        
        TelaPainelCompromissos telaPainelCompromissos = (TelaPainelCompromissos) telaHome
                .acessarSubMenu(SubMenu.PAINEL_COMPROMISSO);

        if (telaPainelCompromissos.abriuPainelCompromissos()) {
            if (telaPainelCompromissos.pegarQtdDeEventosPendentes() > 0) {
                return true;
            }
        }
        return false;
    }

    public void logout() {
        log.debug("Efetuando logoff");
        
        telaLogin.logout();
    }

}

package br.com.galgo.testes.recursos_comuns.teste;

import br.com.galgo.testes.recursos_comuns.categoria.CategoriaDownload;
import br.com.galgo.testes.recursos_comuns.categoria.CategoriaUpload;
import br.com.galgo.testes.recursos_comuns.enumerador.Operacao;
import br.com.galgo.testes.recursos_comuns.enumerador.config.Ambiente;
import br.com.galgo.testes.recursos_comuns.enumerador.menu.SubMenu;
import br.com.galgo.testes.recursos_comuns.enumerador.usuario.Categoria;
import br.com.galgo.testes.recursos_comuns.enumerador.usuario.Papel;
import br.com.galgo.testes.recursos_comuns.enumerador.usuario.UsuarioConfig;
import br.com.galgo.testes.recursos_comuns.exception.ErroAplicacao;
import br.com.galgo.testes.recursos_comuns.file.entidades.Usuario;
import br.com.galgo.testes.recursos_comuns.pageObject.TelaGalgo;
import br.com.galgo.testes.recursos_comuns.pageObject.TelaHome;
import br.com.galgo.testes.recursos_comuns.pageObject.TelaLogin;
import br.com.galgo.testes.recursos_comuns.pageObject.consulta.TelaConsultaFundo;
import br.com.galgo.testes.recursos_comuns.pageObject.consulta.TelaDetalhesFundos;
import br.com.galgo.testes.recursos_comuns.pageObject.fundo.TelaInclusaoFundo;
import br.com.galgo.testes.recursos_comuns.pageObject.upload.TelaResultadoUploadFundo;
import br.com.galgo.testes.recursos_comuns.pageObject.upload.TelaUploadFundo;
import br.com.galgo.testes.recursos_comuns.utils.ConstantesTestes;
import br.com.galgo.testes.recursos_comuns.utils.TesteUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TesteConsultaFundos extends Teste {

	private static final Logger log = LoggerFactory.getLogger(TesteConsultaFundos.class);
	private final String PASTA_TESTE = "Fundos";
	private TelaLogin telaLogin;
	private Ambiente ambiente;
	private String pathUpload;
	private Operacao operacao;

	@Before
	public void setUp() {
		configurar();
	}

	public void configurar() {
		ambiente = TesteUtils.configurarTeste(Ambiente.PRODUCAO, PASTA_TESTE);
		configurar(ConstantesTestes.PATH_ARQUIVO_UPLOAD_COM_ERRO, null,
				ambiente);
	}

	public void configurar(String pathUpload, Operacao operacao,
			Ambiente ambiente) {
		TelaGalgo.abrirBrowser();
		telaLogin = TelaLogin.irParaTelaLogin();

		this.ambiente = ambiente;
		this.pathUpload = pathUpload;
		this.operacao = operacao;
	}

	@Test
	public void testeConsultaFundosAtivoAnbid() throws ErroAplicacao {
		this.setNomeTeste("testeConsultaFundosAtivoAnbid.png");
		final UsuarioConfig usuarioConfig = UsuarioConfig.fromCategoria(
				ambiente, Categoria.USUARIO_FINAL, Papel.AUTO_REGULADOR);
		Usuario usuario = new Usuario(usuarioConfig);

		consultaFundosAtivo(usuario);
	}

	@Category(CategoriaDownload.class)
	@Test
	public void testeDownloadFundosAtivoAnbid() throws ErroAplicacao {
		this.setNomeTeste("testeDownloadFundosAtivoAnbid.png");
		final UsuarioConfig usuarioConfig = UsuarioConfig.fromCategoria(
				ambiente, Categoria.USUARIO_FINAL, Papel.AUTO_REGULADOR);
		Usuario usuario = new Usuario(usuarioConfig);

		TelaDetalhesFundos telaDetalhesFundos = consultaFundosAtivo(usuario);
		TelaConsultaFundo telaFundo = telaDetalhesFundos.voltar();

		telaFundo.consultarFundosViaArquivo(usuario,
				ConstantesTestes.PATH_DOWNLOAD_FUNDOS);
	}

	@Test
	public void testeConsultaFundosSemFiltro() throws ErroAplicacao {
		this.setNomeTeste("testeConsultaFundosSemFiltro.png");

		final UsuarioConfig usuarioConfig = UsuarioConfig.fromCategoria(
				ambiente, Categoria.USUARIO_FINAL, Papel.AUTO_REGULADOR);
		Usuario usuario = new Usuario(usuarioConfig);

		consultaFundos(usuario);
	}

	@Category(CategoriaUpload.class)
	@Test
	public void testeUploadFundos() {
		this.setNomeTeste("testeUploadFundos.png");

		final UsuarioConfig usuarioConfig = UsuarioConfig.fromCategoria(
				ambiente, Categoria.USUARIO_FINAL, Papel.ADMINISTRADOR);

		Usuario usuario = new Usuario(usuarioConfig);

		TelaHome telaHome = telaLogin.loginAs(usuario);

		TelaInclusaoFundo telaFundo = (TelaInclusaoFundo) telaHome
				.acessarSubMenu(SubMenu.INCLUSAO_FUNDOS_INVESTIMENTO);

		//TODO Remover este try catch qdo este teste estiver estabilizado
		try {
			TelaUploadFundo telaUploadFundo = telaFundo.clicarBotaoUpload();

			TelaResultadoUploadFundo telaResultadoUploadFundo = telaUploadFundo
					.incluirArquivo(usuario.getSenha(), pathUpload);
			if (operacao != null) {
				telaResultadoUploadFundo.validaUploadSucesso();
			} else {
				telaResultadoUploadFundo.validaUploadComErro();
			}
		} catch (Exception e) {
			log.error("Este erro não causa a falha do teste provisoriamente", e);
		}
	}

	private void consultaFundos(Usuario usuario) {
		TelaHome telaHome = telaLogin.loginAs(usuario);
		TelaConsultaFundo telaFundo = (TelaConsultaFundo) telaHome
				.acessarSubMenu(SubMenu.CONSULTA_FUNDOS_INVESTIMENTO);
		telaFundo.clicarBotaoConfirmar();
		telaFundo.verificarItemEncontrado();
	}

	private TelaDetalhesFundos consultaFundosAtivo(Usuario usuario) {
		TelaHome telaHome = telaLogin.loginAs(usuario);
		TelaConsultaFundo telaFundo = (TelaConsultaFundo) telaHome
				.acessarSubMenu(SubMenu.CONSULTA_FUNDOS_INVESTIMENTO);

		telaFundo.consultarFundosAtivosAnbid();
		TelaDetalhesFundos telaDetalhesFundos = telaFundo.consultarDetalhes();
		return telaDetalhesFundos;
	}

	@After
	public void tearDown() {
		TesteUtils.finalizarTeste(this.getNomeTeste());
	}

}

package br.com.galgo.testes.recursos_comuns.utils;

import br.com.galgo.testes.recursos_comuns.config.ConfiguracaoSistema;
import br.com.galgo.testes.recursos_comuns.config.ConfiguracaoSuiteTeste;
import br.com.galgo.testes.recursos_comuns.enumerador.config.Ambiente;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SuiteUtils {

    private static final Logger log = LoggerFactory.getLogger(SuiteUtils.class);

    public static void configurarSuiteDefault(Ambiente ambiente, String pastaSuite) {
        boolean usarAmbienteEvidenciaSuite = true;
        boolean usarCaminhoEvidenciaSuite = true;
        boolean concatenarPastaSuiteETeste = false;

        log.debug("ambiente = {}", ambiente);

        ConfiguracaoSuiteTeste configuracaoSuiteTeste = new ConfiguracaoSuiteTeste(ambiente, pastaSuite,
                usarAmbienteEvidenciaSuite, usarCaminhoEvidenciaSuite, concatenarPastaSuiteETeste);

        ConfiguracaoSistema.setConfiguracaoSuiteTeste(configuracaoSuiteTeste);
    }

}

package br.com.galgo.testes.recursos_comuns.utils;

import br.com.galgo.testes.recursos_comuns.config.ConfiguracaoSistema;
import br.com.galgo.testes.recursos_comuns.config.ConfiguracaoTeste;
import br.com.galgo.testes.recursos_comuns.enumerador.config.Ambiente;
import br.com.galgo.testes.recursos_comuns.file.ArquivoUtils;
import br.com.galgo.testes.recursos_comuns.pageObject.TelaGalgo;

public class TesteUtils {

    public static void configurar(Ambiente ambiente, String pastaSuite) {
        ConfiguracaoTeste configuracaoTeste = new ConfiguracaoTeste(ambiente, pastaSuite);
        ConfiguracaoSistema.setConfiguracaoTeste(configuracaoTeste);
    }

    public static Ambiente configurarTeste(Ambiente ambiente, String pastaSuite) {
        configurar(ambiente, pastaSuite);

        if (ConfiguracaoSistema.configuracaoSuiteTeste != null) {
            ambiente = ConfiguracaoSistema.configuracaoSuiteTeste.getAmbiente();
        }

        return ambiente;
    }

    public static void finalizarTeste(String nomeArquivo) {
        try {
            tirarPrint(nomeArquivo);
        } finally {
            TelaGalgo.logout();
        }
    }

    public static void finalizarTesteSemImprimir(String nomeArquivo) {
        TelaGalgo.logout();
    }

    /**
     * Método tirar print da tela. Responsavel por efetuar o printscreen de uma tela.
     * O método recebe um nome de arquivo como parâmatro, o nome é alterado, da seguinte forma:
     * nomeArquivo_dd_mm_aaa_hh_mm_ss.png. 
     * Devido a alteracao do nome, o mesmo é retornado pela função caso haja necessidade da utilização 
     * do print de tela.  
     * @param nomeArquivo o nome do arquivo 
     * @return o nome alterado do arqruivo no formato nomeArquivo_dd_mm_aaa_hh_mm_ss.png.
     */
    public static String tirarPrint(String nomeArquivo) {
        nomeArquivo = ArquivoUtils.getPathConsulta(nomeArquivo, ".png");
        String caminhoEvidencia = ConfiguracaoSistema.getCaminhoEvidenciaTarget(nomeArquivo);
        TelaGalgo.tirarScreenShot(caminhoEvidencia);
        return nomeArquivo;
    }

}

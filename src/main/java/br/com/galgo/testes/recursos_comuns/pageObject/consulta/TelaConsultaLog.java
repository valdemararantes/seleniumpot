package br.com.galgo.testes.recursos_comuns.pageObject.consulta;

import br.com.galgo.testes.recursos_comuns.enumerador.config.TipoAtributo;
import br.com.galgo.testes.recursos_comuns.enumerador.config.TipoCampo;
import br.com.galgo.testes.recursos_comuns.enumerador.filtro.FiltroLog;
import br.com.galgo.testes.recursos_comuns.enumerador.filtro.camposTela.CampoFiltroEntidade;
import br.com.galgo.testes.recursos_comuns.enumerador.filtro.camposTela.CampoFiltroLog;
import br.com.galgo.testes.recursos_comuns.enumerador.filtro.camposTela.CampoFiltroUsuario;
import br.com.galgo.testes.recursos_comuns.pageObject.TelaGalgo;
import br.com.galgo.testes.recursos_comuns.pageObject.TelaUsuario;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TelaConsultaLog extends TelaGalgo {

    private static final Logger log = LoggerFactory.getLogger(TelaConsultaLog.class);

    @Override
    public void clicarBotaoConfirmar() {
        clicarItemPorXPath("/html/body/div[2]/table/tbody/tr[2]/td[2]/table/tbody/tr[3]/td/table/tbody"
            + "/tr/td[2]/div/table/tbody/tr/td/table/tbody/tr/td/div/form[1]/table/tbody" + "/tr[10]/td[2]/input[2]");
    }

    public void incluirFiltro(CampoFiltroLog filtro, String valorCampoInput) {
        FiltroLog filtroLog = filtro.getFiltro();
        if (log.isDebugEnabled()) {
            String filtroNome = StringUtils.isBlank(filtro.getDescricao()) ? filtroLog.toString() :
                                filtro.getDescricao();
            log.debug("Configurando o filtro {} com o valor {}", filtroNome, valorCampoInput);
        }
        if (filtroLog.getTipoCampo() == TipoCampo.RADIO) {
            clicarItemPorId(filtro.getCampo());
        } else if (filtroLog.getTipoCampo() == TipoCampo.SELECT) {
            preencherCampoSelect(filtro.getDescricao());
        } else if (filtroLog.getTipoCampo() == TipoCampo.INPUT) {
            try {
                preencheCampoPorId(filtro.getCampo(), valorCampoInput);
            } catch (Exception e) {
                preencheCampoXPath(filtro.getCampo(), valorCampoInput);
            }
        } else if (filtroLog.getTipoCampo() == TipoCampo.BUSCA) {
            clicarCampoBusca(filtro);
        }
    }

    public void incluirFiltro(CampoFiltroLog filtro) {
        incluirFiltro(filtro, "");
    }

    public void incluirFiltroBuscaEntidade(CampoFiltroEntidade filtroEntidade, String valorCampoInput) {
        log.debug("filtroEntidade={}; valorCampoInput={}", filtroEntidade.toString_ForLog(), valorCampoInput);
        TelaConsultaEntidade telaEntidade = entrarFiltroBuscaEntidade();
        telaEntidade.incluirFiltro(filtroEntidade, valorCampoInput);
        telaEntidade.clicarBotaoConfirmar();
        telaEntidade.escolherPrimeiroItem();
    }

    public void incluirFiltroBuscaUsuario(CampoFiltroLog campoFiltroLog, CampoFiltroUsuario filtro,
        String valorCampoInput) {
        try {
            log.debug("Início...");
            TelaUsuario telaUsuario = entrarFiltroBuscaUsuario(campoFiltroLog);
            log.debug("Preenchendo o campo \"Login do Usuário\"");
            telaUsuario.incluirFiltro(filtro, valorCampoInput);
            log.debug("Confirmando para buscar o usuário");
            telaUsuario.clicarBotaoConfirmar();
            log.debug("Seleciona o primeiro usuário da lista");
            telaUsuario.escolherPrimeiroItem();
            log.debug("Retornando");
        } catch (Exception e) {
            log.error(null, e);
            gerarEvidenciasDeErroParaLog("TelaConsultaLog_clicarCampoBusca");
            throw new RuntimeException(e);
        }
    }

    private void clicarCampoBusca(CampoFiltroLog filtro) {
        if (filtro.getTipoAtributo() == TipoAtributo.ID) {
            clicarItemPorId(filtro.getCampo());
        } else if (filtro.getTipoAtributo() == TipoAtributo.NAME) {
            clicarItemPorName(filtro.getCampo());
        } else if (filtro.getTipoAtributo() == TipoAtributo.XPATH) {
            clicarItemPorXPath(filtro.getCampo());
        }
    }

    private TelaConsultaEntidade entrarFiltroBuscaEntidade() {
        incluirFiltro(CampoFiltroLog.BUSCA_ENTIDADE);
        return new TelaConsultaEntidade();
    }

    private TelaUsuario entrarFiltroBuscaUsuario(CampoFiltroLog campoFiltroLog) {
        log.debug("Acessando a tela \"Busca de Usuário\"");
        incluirFiltro(campoFiltroLog);
        return new TelaUsuario();
    }
}

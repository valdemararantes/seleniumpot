package br.com.galgo.testes.recursos_comuns.pageObject.consulta;

import br.com.galgo.testes.recursos_comuns.enumerador.Entidade;
import br.com.galgo.testes.recursos_comuns.enumerador.config.TipoCampo;
import br.com.galgo.testes.recursos_comuns.enumerador.filtro.FiltroEntidade;
import br.com.galgo.testes.recursos_comuns.enumerador.filtro.camposTela.CampoFiltroEntidade;
import br.com.galgo.testes.recursos_comuns.pageObject.TelaGalgo;
import br.com.galgo.testes.recursos_comuns.pageObject.TelaHome;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.profiler.Profiler;

import java.util.Objects;

public class TelaConsultaEntidade extends TelaGalgo {

    private static final Logger log = LoggerFactory.getLogger(TelaConsultaEntidade.class);


    public WebElement consultarEntidadePJ(Entidade entidade) {
        log.debug("Início do método");
        Objects.requireNonNull(entidade);
        incluirFiltro(CampoFiltroEntidade.RADIO_PJ);
        try {
            incluirFiltro(CampoFiltroEntidade.INPUT_NOME_FANTASIA, entidade.getDescricao());
        } catch (Exception e) {
            log.error("Erro ao incluir filtro nome fantasia com valor {}", entidade.getDescricao());
        }

        buscar();

        WebElement gridElement = encontrarCampoPorCss("body > div#FLYParent > table");
        log.debug("Fim do método");
        return gridElement;
    }

    public void consultarTodasEntidadesPJ(TelaHome telaHome) {
        incluirFiltro(CampoFiltroEntidade.RADIO_PJ);
        buscar();
    }

    public void escolherPrimeiroItem() {
        try {
            clicarItemPorName(getNamePrimeiroItemBusca());
        } catch (Exception e) {
            clicarItemPorXPath("/html/body/div[2]/table/tbody/tr[2]/td[2]/table/tbody/tr[3]/td/table/tbody/tr/td[2" +
                               "]/div/table/tbody/tr/td/table/tbody/tr/td/div/form/table[3]/tbody/tr[1]/td/div/span[1]/div[2"
                               +
                               "]/input");
        }

        try {
            clicarItemPorId(getIdBotaoConfirmaResultado());
        } catch (Exception e) {
            clicarItemPorXPath("/html/body/div[2]/table/tbody/tr[2]/td[2]/table/tbody/tr[3]/td/table/tbody/tr/td[2"
                               + "]/div/table/tbody/tr/td/table/tbody/tr/td/div/form/table[6]/tbody/tr[2]/td/input[2]");
        }
    }

    public void filtrarPorNomeFantasia(String nomeFantasia) {
        incluirFiltro(CampoFiltroEntidade.INPUT_NOME_FANTASIA, nomeFantasia);
        clicarBotaoConfirmar();
        escolherPrimeiroItem();
    }

    public void incluirFiltro(CampoFiltroEntidade filtro, String valorCampoInput) {
        FiltroEntidade filtroEntidade = filtro.getFiltro();
        if (filtroEntidade.getTipoCampo() == TipoCampo.RADIO) {
            if (verificaTextoNaTela(filtro.getCampo())) {
                clicarItemPorId(filtro.getCampo());
            } else {
                clicarItemPorXPath(filtro.getCampo());
            }
        } else if (filtroEntidade.getTipoCampo() == TipoCampo.SELECT) {
            preencherCampoSelect(filtro.getDescricao());
        } else if (filtroEntidade.getTipoCampo() == TipoCampo.INPUT) {
            if (verificaTextoNaTela(filtro.getCampo())) {
                preencheCampoPorId(filtro.getCampo(), valorCampoInput);
            } else {
                preencheCampoXPath(filtro.getCampo(), valorCampoInput);
            }
        }
    }

    public void incluirFiltro(CampoFiltroEntidade filtro) {
        incluirFiltro(filtro, "");
    }

    private void buscar() {
        Profiler profiler = new Profiler("BUSCAR_ENTIDADE_ANBIMA");
        profiler.setLogger(log);
        log.debug("Clicando no botão Confirmar para buscar as entidades e cronometrando");
        profiler.start("clicarItemPorCss");
        clicarItemPorCss("input.submit_ativo[onclick*=iniciarBusca]");
        profiler.start("esperarTelaCarregar");
        esperarTelaCarregar();
        log.debug("Validando se trouxe as entidades");
        profiler.start("verificarItemEncontrado");
        verificarItemEncontrado();
        profiler.stop().log();
        log.debug("Validado. Trouxe as entidades");
    }

    private String getIdBotaoConfirmaResultado() {
        return "ConfirmarResultado";
    }

    private String getNamePrimeiroItemBusca() {
        return "linhasChecked";
    }

}

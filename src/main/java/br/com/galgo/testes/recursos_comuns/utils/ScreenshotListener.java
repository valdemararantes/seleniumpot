package br.com.galgo.testes.recursos_comuns.utils;

import br.com.galgo.testes.recursos_comuns.pageObject.TelaGalgo;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.SystemUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.TestListenerAdapter;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

/**
 * Created by valdemar.arantes on 01/06/2015.
 */

public class ScreenshotListener extends TestListenerAdapter {

    private static final Logger log = LoggerFactory.getLogger(ScreenshotListener.class);

    @Override
    public void onStart(ITestContext testContext) {
        super.onStart(testContext);
        log.debug("testContext = {}", testContext);
    }

    @Override
    public void onTestFailure(ITestResult tr) {
        try {
            super.onTestFailure(tr);
            String testName = tr.getTestClass().getRealClass().getSimpleName() + "." + tr.getName();
            log.warn("Falha no teste {} [{}]", Duration.of(tr.getEndMillis() - tr.getEndMillis(), ChronoUnit.MILLIS), testName);
            if (TelaGalgo.driver == null) {
                log.debug("WebDriver nulo. Retornando");
                return;
            }
            String nowStr = LocalDateTime.now().toString();
            nowStr = StringUtils.replace(nowStr, ":", "-");
            String screenshotFile = String.format(SystemUtils.getUserDir() + "/target/evidencia/erro_%s_%s.png", testName, nowStr);
            TelaGalgo.tirarScreenShot(screenshotFile);
        } catch (Exception e) {
            log.error(null, e);
        } finally {
            //TelaGalgo.fecharBrowser();
        }
    }

    @Override
    public void onTestSuccess(ITestResult tr) {
        try {
            super.onTestSuccess(tr);
            String testName = tr.getTestClass().getRealClass().getSimpleName() + "." + tr.getName();
            log.trace("Sucesso no teste {} [{}]", testName, Duration.of(tr.getEndMillis() - tr.getStartMillis(), ChronoUnit.MILLIS));
            if (TelaGalgo.driver == null) {
                log.debug("WebDriver nulo. Retornando");
                return;
            }
            String nowStr = LocalDateTime.now().toString();
            nowStr = StringUtils.replace(nowStr, ":", "-");
            String screenshotFile = String.format("target/evidencia/%s_%s.png", testName, nowStr);
            TelaGalgo.tirarFotoDaTela(null /*(WebElement) tr.getTestContext().getAttribute("elementToPrint")*/, screenshotFile);
        } catch (Exception e) {
            log.error(null, e);
        } finally {
            //TelaGalgo.fecharBrowser();
        }
    }


}

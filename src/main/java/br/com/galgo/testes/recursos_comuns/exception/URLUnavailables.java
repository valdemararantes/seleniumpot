package br.com.galgo.testes.recursos_comuns.exception;

import com.google.common.collect.Sets;

import java.util.Set;

/**
 * Classe que mantém uma lista das URLs indisponíveis
 * <p>
 * Created by valdemar.arantes on 03/07/2015.
 */
public class URLUnavailables {
    private static Set<String> urls = Sets.newConcurrentHashSet();

    /**
     * Adiciona uma URL à lista das URLs indisponíveis
     *
     * @param url
     */
    public static void addURL(String url) {
        urls.add(url);
    }

    /**
     * Verifica se uma URL está indisponível
     *
     * @param url
     * @return True caso a URL esteja indisponível
     */
    public static boolean isUnavailable(String url) {
        return urls.contains(url);
    }
}

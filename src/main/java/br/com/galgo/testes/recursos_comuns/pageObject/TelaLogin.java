package br.com.galgo.testes.recursos_comuns.pageObject;

import br.com.galgo.testes.recursos_comuns.enumerador.config.Ambiente;
import br.com.galgo.testes.recursos_comuns.exception.MensagemErro;
import br.com.galgo.testes.recursos_comuns.file.entidades.Usuario;
import br.com.galgo.testes.recursos_comuns.teste.Teste;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.profiler.Profiler;

public class TelaLogin extends TelaGalgo {

    private static final Logger log = LoggerFactory.getLogger(TelaLogin.class);

    private TelaLogin() {
    }

    public static TelaLogin irParaTelaLogin() {
        log.debug("Esperando a thread {} terminar", Teste.futureBrowserOpened);
        if (!Teste.futureBrowserOpened.isDone()) {
            try {
                Teste.futureBrowserOpened.get();
            } catch (Exception e) {
                log.error(null, e);
                org.testng.Assert.fail(e.toString());
            }
        }
        WebDriverWait wait = new WebDriverWait(driver, 10000);

/*
        log.debug("Tirando uma foto da tela...");
        String caminhoEvidencia = ConfiguracaoSistema.getCaminhoEvidenciaTarget("teste_neto.png");
        log.debug("Caminho da foto: {}", caminhoEvidencia);
        TelaGalgo.tirarScreenShot(caminhoEvidencia);
*/

        log.debug("Localizando o elemento com id nmUsuario...");
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("nmUsuario")));
        log.debug("Thread {} terminou. Seguindo", Teste.futureBrowserOpened);
        return new TelaLogin();
    }

    public TelaHome loginAs(Usuario usuario) {

        String login = usuario.getLogin();
        String senha = usuario.getSenha();

        log.info("Realizando o login com usuário {}", login);
        Profiler profiler = new Profiler("LOGIN");
        profiler.setLogger(log);
        preencherCampoLogin(login);
        preencherCampoSenha(senha);

        if (!submeterFormulario(usuario)) {
            Assert.fail(MensagemErro.ERRO_LOGIN_INVALIDO + "[usuario=" + usuario.getLogin() + "]");
        }

        profiler.stop().log();
        log.debug("Instanciando TelaHome");
        
        esperarTelaCarregar();//TODO ecalderini: nao eh necessario
        
        return new TelaHome(usuario);
    }

    private String getCampoSenha() {
        return "dsSenha";
    }

    private String getClassNameTeclaEnter(Ambiente ambiente) {
        return "submit_ativo";
    }

    private String getXPathUsuarioBloqueado() {
        return "/html/body/div[2]/table/tbody/tr[2]/td[2]/table/tbody/tr[3]/td/table/tbody/tr/td[2]/div/table/tbody"
                + "/tr[1]/td/table/tbody/tr/td[1]/table/tbody/tr[1]/td/div/form/table/tbody/tr[1]/td/div/div/table"
                + "/tbody/tr[1]/td[2]/ul/li";
    }

    private void preencherCampoLogin(String login) {
        preencheCampoPorId("nmUsuario", login);
    }

    private void preencherCampoSenha(String senha) {
        String getCampoSenha = getCampoSenha();
        preencheCampoPorId(getCampoSenha, senha);
    }

    private boolean submeterFormulario(Usuario usuario) {
        Ambiente ambiente = usuario.getAmbiente();

        log.debug("Clicando em Confirmar para logar no Galgo");
        clicarItemPorClassName(getClassNameTeclaEnter(ambiente));
        esperarTelaCarregar(); //TODO ecalderini: nao eh necessario
        
        final String stringToFind = "Prezado(a) Usuário(a)";
        if (verificaTextoNaTela(stringToFind)) {
            log.debug("Texto que inicia com {} encontrado. Login realizado sem necessidade de troca de senha",
                    stringToFind);
            return true;
        }

        log.debug("Validando se está na tela de troca de senha");
        TelaTrocaSenha telaTrocaSenha = new TelaTrocaSenha();
        if (telaTrocaSenha.estaNaTeladeTrocaDeSenha()) {
            return telaTrocaSenha.trocarSenha(usuario);
        }
        if (verificaCampoVisivelPorXPath(getXPathUsuarioBloqueado(), 3)) {
            return false;
        }
        return true;
    }

}

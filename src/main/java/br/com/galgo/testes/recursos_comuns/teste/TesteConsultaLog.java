package br.com.galgo.testes.recursos_comuns.teste;

import br.com.galgo.testes.recursos_comuns.enumerador.filtro.camposTela.CampoFiltroLog;
import br.com.galgo.testes.recursos_comuns.enumerador.filtro.camposTela.CampoFiltroUsuario;
import br.com.galgo.testes.recursos_comuns.enumerador.menu.SubMenu;
import br.com.galgo.testes.recursos_comuns.enumerador.usuario.Categoria;
import br.com.galgo.testes.recursos_comuns.enumerador.usuario.Papel;
import br.com.galgo.testes.recursos_comuns.enumerador.usuario.UsuarioConfig;
import br.com.galgo.testes.recursos_comuns.exception.ErroAplicacao;
import br.com.galgo.testes.recursos_comuns.file.entidades.Usuario;
import br.com.galgo.testes.recursos_comuns.pageObject.TelaHome;
import br.com.galgo.testes.recursos_comuns.pageObject.TelaLogin;
import br.com.galgo.testes.recursos_comuns.pageObject.consulta.TelaConsultaLog;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.CompletableFuture;

public class TesteConsultaLog extends Teste {

    private static final Logger log = LoggerFactory.getLogger(TesteConsultaLog.class);
    private final String PASTA_TESTE = "Log";

    @Test
    public void testeConsultaDeLog() throws ErroAplicacao {
        this.setNomeTeste("testeConsultaLog.png");
        Usuario usuario = null;

        CompletableFuture<Usuario> futureUsuario = CompletableFuture.supplyAsync(() -> {
            final UsuarioConfig usuarioConfig = UsuarioConfig.fromCategoria(ambiente, Categoria.USUARIO_FINAL,
                    Papel.ADM_SISTEMA);
            return new Usuario(usuarioConfig);
        });
        try {
            log.debug("Recuperando o usuário");
            usuario = futureUsuario.get();
        } catch (Exception e) {
            log.error(null, e);
            Assert.fail("Erro ao abrir o browser: " + e.getMessage());
        }

        try {
            TelaHome telaHome = TelaLogin.irParaTelaLogin().loginAs(usuario);
            TelaConsultaLog telaConsultaDeLog = (TelaConsultaLog) telaHome.acessarSubMenu(SubMenu.CONSULTA_DE_LOG);

            final LocalDateTime now = LocalDateTime.now(ZoneId.of(ZoneId.SHORT_IDS.get("BET")));
            final LocalDateTime almostNow = now.minusMinutes(5);
            String dataInicial = DateTimeFormatter.ofPattern("dd/MM/yyyy").format(almostNow);
            String horaInicial = DateTimeFormatter.ofPattern("HH:mm").format(almostNow);

            //Date dataFiltro = DataUtils.subtrairMinutos(ConfiguracaoSistema.MINUTOS_ANTES_LOGIN, new Date());
            //String dataInicial = FormatoData.DD_MM_YYYY.formata(dataFiltro);
            //String horaInicial = FormatoData.HH_MM.formata(dataFiltro);

            telaConsultaDeLog.incluirFiltro(CampoFiltroLog.INPUT_DATA_INICIAL, dataInicial);
            telaConsultaDeLog.incluirFiltro(CampoFiltroLog.INPUT_HORA_INICIAL, horaInicial);

            telaConsultaDeLog.incluirFiltroBuscaUsuario(CampoFiltroLog.BUSCA_USUARIO,
                    CampoFiltroUsuario.INPUT_LOGIN_USUARIO, usuario.getLogin());

            telaConsultaDeLog.clicarBotaoConfirmar();
            telaConsultaDeLog.verificarItemEncontrado();
        } catch (Exception e) {
            log.error(null, e);
            throw new RuntimeException(e);
        }
    }
}

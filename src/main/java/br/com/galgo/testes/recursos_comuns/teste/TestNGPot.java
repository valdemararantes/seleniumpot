package br.com.galgo.testes.recursos_comuns.teste;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;

import java.util.concurrent.ExecutionException;

/**
 * Created by Valdemar on 23/06/2015.
 */
public class TestNGPot extends Teste {

    private static final Logger log = LoggerFactory.getLogger(TestNGPot.class);


    @Test
    public void test1() {
        log.debug("Início");
        try {
            Teste.futureBrowserOpened.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test2() {
        log.debug("Início");
    }

}

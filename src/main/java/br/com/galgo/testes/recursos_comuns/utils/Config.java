package br.com.galgo.testes.recursos_comuns.utils;

import org.apache.commons.configuration.*;
import org.apache.commons.configuration.event.ConfigurationErrorListener;
import org.apache.commons.configuration.event.ConfigurationListener;
import org.apache.commons.configuration.interpol.ConfigurationInterpolator;
import org.apache.commons.configuration.reloading.ReloadingStrategy;
import org.apache.commons.lang.SystemUtils;
import org.apache.commons.lang.text.StrSubstitutor;
import org.apache.commons.logging.Log;

import java.io.*;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.URL;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

/**
 * Created by valdemar.arantes on 03/06/2015.
 */
public class Config {
    private static PropertiesConfiguration config;

    static {
        try {
            config = new PropertiesConfiguration(SystemUtils.getUserHome() + "/.testes_automatizados/config.properties");
            config.setAutoSave(true);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Gets the property value for including other properties files.
     * By default it is "include".
     *
     * @return A String.
     */
    public static String getInclude() {
        return PropertiesConfiguration.getInclude();
    }

    /**
     * Sets a new value for the specified property. This implementation checks
     * if the auto save mode is enabled and saves the configuration if
     * necessary.
     *  @param key the key of the affected property
     * @param value the value
     */
    public static void setProperty(String key, Object value) {
        config.setProperty(key, value);
    }

    /**
     * Removes the specified event listener so that it does not receive any
     * further events caused by this object.
     *
     * @param l the listener to be removed
     * @return a flag whether the event listener was found
     */
    public static boolean removeConfigurationListener(ConfigurationListener l) {
        return config.removeConfigurationListener(l);
    }

    public static double getDouble(String key) {
        return config.getDouble(key);
    }

    public static boolean containsKey(String key) {
        return config.containsKey(key);
    }

    /**
     * Returns the {@code IOFactory} to be used for creating readers and
     * writers when loading or saving this configuration.
     *
     * @return the {@code IOFactory}
     * @since 1.7
     */
    public static PropertiesConfiguration.IOFactory getIOFactory() {
        return config.getIOFactory();
    }

    /**
     * Sets the associated layout object.
     *
     * @param layout the new layout object; can be <b>null</b>, then a new
     * layout object will be created
     * @since 1.3
     */
    public static void setLayout(PropertiesConfigurationLayout layout) {
        config.setLayout(layout);
    }

    /**
     * Save the configuration to the specified URL.
     * This doesn't change the source of the configuration, use setURL()
     * if you need it.
     *
     * @param url the URL
     *
     * @throws ConfigurationException if an error occurs during the save operation
     */
    public static void save(URL url) throws ConfigurationException {
        config.save(url);
    }

    /**
     * Save the configuration to the specified file. This doesn't change the
     * source of the configuration, use setFileName() if you need it.
     *
     * @param fileName the file name
     *
     * @throws ConfigurationException if an error occurs during the save operation
     */
    public static void save(String fileName) throws ConfigurationException {
        config.save(fileName);
    }

    public static List<Object> getList(String key, List<?> defaultValue) {
        return config.getList(key, defaultValue);
    }

    public static Byte getByte(String key, Byte defaultValue) {
        return config.getByte(key, defaultValue);
    }

    public static int getInt(String key) {
        return config.getInt(key);
    }

    /**
     * Removes the specified error listener so that it does not receive any
     * further events caused by this object.
     *
     * @param l the listener to remove
     * @return a flag whether the listener could be found and removed
     * @since 1.4
     */
    public static boolean removeErrorListener(ConfigurationErrorListener l) {
        return config.removeErrorListener(l);
    }

    /**
     * {@inheritDoc}
     * @see #setThrowExceptionOnMissing(boolean)
     * @param key
     */
    public static BigDecimal getBigDecimal(String key) {
        return config.getBigDecimal(key);
    }

    public static Float getFloat(String key, Float defaultValue) {
        return config.getFloat(key, defaultValue);
    }

    public static Object getProperty(String key) {
        return config.getProperty(key);
    }

    /**
     * Adds a special
     * {@link ConfigurationErrorListener}
     * object to this configuration that will log all internal errors. This
     * method is intended to be used by certain derived classes, for which it is
     * known that they can fail on property access (e.g.
     * {@code DatabaseConfiguration}).
     *
     * @since 1.4
     */
    public static void addErrorLogListener() {
        config.addErrorLogListener();
    }

    /**
     * Adds a new property to this configuration. This implementation checks if
     * the auto save mode is enabled and saves the configuration if necessary.
     *  @param key the key of the new property
     * @param value the value
     */
    public static void addProperty(String key, Object value) {
        config.addProperty(key, value);
    }

    /**
     * Set whether this configuration should use delimiters when parsing
     * property values to convert them to lists of values. By default delimiter
     * parsing is enabled
     *
     * Note: this change will only be effective for new parsings. If you
     * want it to take effect for all loaded properties use the no arg constructor
     * and call this method before setting source.
     * @param delimiterParsingDisabled a flag whether delimiter parsing should
     * be disabled
     */
    public static void setDelimiterParsingDisabled(boolean delimiterParsingDisabled) {
        config.setDelimiterParsingDisabled(delimiterParsingDisabled);
    }

    /**
     * Performs a reload operation if necessary. This method is called on each
     * access of this configuration. It asks the associated reloading strategy
     * whether a reload should be performed. If this is the case, the
     * configuration is cleared and loaded again from its source. If this
     * operation causes an exception, the registered error listeners will be
     * notified. The error event passed to the listeners is of type
     * {@code EVENT_RELOAD} and contains the exception that caused the
     * event.
     */
    public static void reload() {
        config.reload();
    }

    /**
     * Reports the status of file inclusion.
     *
     * @return True if include files are loaded.
     */
    public static boolean isIncludesAllowed() {
        return config.isIncludesAllowed();
    }

    /**
     * Load the configuration from the specified stream, using the specified
     * encoding. If the encoding is null the default encoding is used.
     *
     * @param in the input stream
     * @param encoding the encoding used. {@code null} to use the default encoding
     *
     * @throws ConfigurationException if an error occurs during the load operation
     */
    public static void load(InputStream in, String encoding) throws ConfigurationException {
        config.load(in, encoding);
    }

    public static Short getShort(String key, Short defaultValue) {
        return config.getShort(key, defaultValue);
    }

    public static void setAutoSave(boolean autoSave) {
        config.setAutoSave(autoSave);
    }

    public static ReloadingStrategy getReloadingStrategy() {
        return config.getReloadingStrategy();
    }

    /**
     * {@inheritDoc}
     * @see #setThrowExceptionOnMissing(boolean)
     * @param key
     */
    public static String getString(String key) {
        return config.getString(key);
    }

    public static Double getDouble(String key, Double defaultValue) {
        return config.getDouble(key, defaultValue);
    }

    /**
     * Allows to set the {@code throwExceptionOnMissing} flag. This
     * flag controls the behavior of property getter methods that return
     * objects if the requested property is missing. If the flag is set to
     * <b>false</b> (which is the default value), these methods will return
     * <b>null</b>. If set to <b>true</b>, they will throw a
     * {@code NoSuchElementException} exception. Note that getter methods
     * for primitive data types are not affected by this flag.
     *
     * @param throwExceptionOnMissing The new value for the property
     */
    public static void setThrowExceptionOnMissing(boolean throwExceptionOnMissing) {
        config.setThrowExceptionOnMissing(throwExceptionOnMissing);
    }

    public static FileSystem getFileSystem() {
        return config.getFileSystem();
    }

    public static Configuration subset(String prefix) {
        return config.subset(prefix);
    }

    public static byte getByte(String key, byte defaultValue) {
        return config.getByte(key, defaultValue);
    }

    /**
     * Returns the full path to the file this configuration is based on. The
     * return value is a valid File path only if this configuration is based on
     * a file on the local disk.
     * If the configuration was loaded from a packed archive the returned value
     * is the string form of the URL from which the configuration was loaded.
     *
     * @return the full path to the configuration file
     */
    public static String getPath() {
        return config.getPath();
    }

    public static double getDouble(String key, double defaultValue) {
        return config.getDouble(key, defaultValue);
    }

    public static boolean reload(boolean checkReload) {
        return config.reload(checkReload);
    }

    /**
     * Set the location of this configuration as a URL. For loading this can be
     * an arbitrary URL with a supported protocol. If the configuration is to
     * be saved, too, a URL with the &quot;file&quot; protocol should be
     * provided.
     *
     * @param url the location of this configuration as URL
     */
    public static void setURL(URL url) {
        config.setURL(url);
    }

    public static float getFloat(String key) {
        return config.getFloat(key);
    }

    /**
     * Copies the content of the specified configuration into this
     * configuration. If the specified configuration contains a key that is also
     * present in this configuration, the value of this key will be replaced by
     * the new value. <em>Note:</em> This method won't work well when copying
     * hierarchical configurations because it is not able to copy information
     * about the properties' structure (i.e. the parent-child-relationships will
     * get lost). So when dealing with hierarchical configuration objects their
     * {@link HierarchicalConfiguration#clone() clone()} methods
     * should be used.
     *
     * @param c the configuration to copy (can be <b>null</b>, then this
     * operation will have no effect)
     * @since 1.5
     */
    public static void copy(Configuration c) {
        config.copy(c);
    }

    /**
     * Load the configuration from the specified URL. If the configuration is
     * already associated with a source, the current source is not changed.
     * Otherwise (i.e. this is the first load operation), the source URL and
     * the base path are set now based on the source to be loaded.
     *
     * @param url the URL of the file to be loaded
     * @throws ConfigurationException if an error occurs
     */
    public static void load(URL url) throws ConfigurationException {
        config.load(url);
    }

    /**
     * Determines whether detail events should be generated. If enabled, some
     * methods can generate multiple update events. Note that this method
     * records the number of calls, i.e. if for instance
     * {@code setDetailEvents(false)} was called three times, you will
     * have to invoke the method as often to enable the details.
     *
     * @param enable a flag if detail events should be enabled or disabled
     */
    public static void setDetailEvents(boolean enable) {
        config.setDetailEvents(enable);
    }

    public static void clearProperty(String key) {
        config.clearProperty(key);
    }

    /**
     * Returns an {@code Iterator} with the keys contained in this
     * configuration. This implementation performs a reload if necessary before
     * obtaining the keys. The {@code Iterator} returned by this method
     * points to a snapshot taken when this method was called. Later changes at
     * the set of keys (including those caused by a reload) won't be visible.
     * This is because a reload can happen at any time during iteration, and it
     * is impossible to determine how this reload affects the current iteration.
     * When using the iterator a client has to be aware that changes of the
     * configuration are possible at any time. For instance, if after a reload
     * operation some keys are no longer present, the iterator will still return
     * those keys because they were found when it was created.
     *
     * @return an {@code Iterator} with the keys of this configuration
     */
    public static Iterator<String> getKeys() {
        return config.getKeys();
    }

    /**
     * Returns the object that is responsible for variable interpolation.
     *
     * @return the object responsible for variable interpolation
     * @since 1.4
     */
    public static StrSubstitutor getSubstitutor() {
        return config.getSubstitutor();
    }

    /**
     * Load the properties from the given reader.
     * Note that the {@code clear()} method is not called, so
     * the properties contained in the loaded file will be added to the
     * actual set of properties.
     *
     * @param in An InputStream.
     *
     * @throws ConfigurationException if an error occurs
     */
    public static void load(Reader in) throws ConfigurationException {
        config.load(in);
    }

    /**
     * Set the file where the configuration is stored. The passed in file is
     * made absolute if it is not yet. Then the file's path component becomes
     * the base path and its name component becomes the file name.
     *
     * @param file the file where the configuration is stored
     */
    public static void setFile(File file) {
        config.setFile(file);
    }

    public static void setReloadingStrategy(ReloadingStrategy strategy) {
        config.setReloadingStrategy(strategy);
    }

    /**
     * Returns a flag whether detail events are enabled.
     *
     * @return a flag if detail events are generated
     */
    public static boolean isDetailEvents() {
        return config.isDetailEvents();
    }

    /**
     * Return the name of the file.
     *
     * @return the file name
     */
    public static String getFileName() {
        return config.getFileName();
    }

    /**
     * Allows to set the logger to be used by this configuration object. This
     * method makes it possible for clients to exactly control logging behavior.
     * Per default a logger is set that will ignore all log messages. Derived
     * classes that want to enable logging should call this method during their
     * initialization with the logger to be used.
     *
     * @param log the new logger
     * @since 1.4
     */
    public static void setLogger(Log log) {
        config.setLogger(log);
    }

    /**
     * Sets the location of this configuration as a full or relative path name.
     * The passed in path should represent a valid file name on the file system.
     * It must not be used to specify relative paths for files that exist
     * in classpath, either plain file system or compressed archive,
     * because this method expands any relative path to an absolute one which
     * may end in an invalid absolute path for classpath references.
     *
     * @param path the full path name of the configuration file
     */
    public static void setPath(String path) {
        config.setPath(path);
    }

    /**
     * Get an array of strings associated with the given configuration key.
     * If the key doesn't map to an existing object, an empty array is returned.
     * If a property is added to a configuration, it is checked whether it
     * contains multiple values. This is obvious if the added object is a list
     * or an array. For strings it is checked whether the string contains the
     * list delimiter character that can be specified using the
     * {@code setListDelimiter()} method. If this is the case, the string
     * is split at these positions resulting in a property with multiple
     * values.
     *
     * @param key The configuration key.
     * @return The associated string array if key is found.
     *
     * @throws ConversionException is thrown if the key maps to an
     *         object that is not a String/List of Strings.
     * @see #setListDelimiter(char)
     * @see #setDelimiterParsingDisabled(boolean)
     */
    public static String[] getStringArray(String key) {
        return config.getStringArray(key);
    }

    /**
     * Sets the default list delimiter.
     *
     * @param delimiter the delimiter character
     * @deprecated Use AbstractConfiguration.setDefaultListDelimiter(char)
     * instead
     */
    @Deprecated
    public static void setDelimiter(char delimiter) {
        AbstractConfiguration.setDelimiter(delimiter);
    }

    /**
     * For configurations extending AbstractConfiguration, allow them to change
     * the listDelimiter from the default comma (","). This value will be used
     * only when creating new configurations. Those already created will not be
     * affected by this change
     *
     * @param delimiter The new listDelimiter
     */
    public static void setDefaultListDelimiter(char delimiter) {
        AbstractConfiguration.setDefaultListDelimiter(delimiter);
    }

    /**
     * Change the list delimiter for this configuration.
     *
     * Note: this change will only be effective for new parsings. If you
     * want it to take effect for all loaded properties use the no arg constructor
     * and call this method before setting the source.
     *
     * @param listDelimiter The new listDelimiter
     */
    public static void setListDelimiter(char listDelimiter) {
        config.setListDelimiter(listDelimiter);
    }

    /**
     * Returns a collection with all configuration error listeners that are
     * currently registered at this object.
     *
     * @return a collection with the registered
     * {@code ConfigurationErrorListener}s (this collection is a
     * snapshot of the currently registered listeners; it cannot be manipulated)
     * @since 1.4
     */
    public static Collection<ConfigurationErrorListener> getErrorListeners() {
        return config.getErrorListeners();
    }

    /**
     * Extend the setBasePath method to turn includes
     * on and off based on the existence of a base path.
     *
     * @param basePath The new basePath to set.
     */
    public static void setBasePath(String basePath) {
        config.setBasePath(basePath);
    }

    /**
     * Save the configuration to the specified stream, using the encoding
     * returned by {@link #getEncoding()}.
     *
     * @param out the output stream
     *
     * @throws ConfigurationException if an error occurs during the save operation
     */
    public static void save(OutputStream out) throws ConfigurationException {
        config.save(out);
    }

    public static void setFileSystem(FileSystem fileSystem) {
        config.setFileSystem(fileSystem);
    }

    /**
     * Returns the {@code ConfigurationInterpolator} object that manages
     * the lookup objects for resolving variables. <em>Note:</em> If this
     * object is manipulated (e.g. new lookup objects added), synchronization
     * has to be manually ensured. Because
     * {@code ConfigurationInterpolator} is not thread-safe concurrent
     * access to properties of this configuration instance (which causes the
     * interpolator to be invoked) may cause race conditions.
     *
     * @return the {@code ConfigurationInterpolator} associated with this
     * configuration
     * @since 1.4
     */
    public static ConfigurationInterpolator getInterpolator() {
        return config.getInterpolator();
    }

    /**
     * Return the URL where the configuration is stored.
     *
     * @return the configuration's location as URL
     */
    public static URL getURL() {
        return config.getURL();
    }

    public static void resetFileSystem() {
        config.resetFileSystem();
    }

    /**
     * Return the comment header.
     *
     * @return the comment header
     * @since 1.1
     */
    public static String getHeader() {
        return config.getHeader();
    }

    /**
     * Returns the logger used by this configuration object.
     *
     * @return the logger
     * @since 1.4
     */
    public static Log getLogger() {
        return config.getLogger();
    }

    /**
     * Removes all registered configuration listeners.
     */
    public static void clearConfigurationListeners() {
        config.clearConfigurationListeners();
    }

    /**
     * Retrieve the delimiter for this configuration. The default
     * is the value of defaultListDelimiter.
     *
     * @return The listDelimiter in use
     */
    public static char getListDelimiter() {
        return config.getListDelimiter();
    }

    public static byte getByte(String key) {
        return config.getByte(key);
    }

    public static boolean isEmpty() {
        return config.isEmpty();
    }

    /**
     * Adds a new configuration error listener to this object. This listener
     * will then be notified about internal problems.
     *
     * @param l the listener to register (must not be <b>null</b>)
     * @since 1.4
     */
    public static void addErrorListener(ConfigurationErrorListener l) {
        config.addErrorListener(l);
    }

    /**
     * Load the configuration from the specified stream, using the encoding
     * returned by {@link #getEncoding()}.
     *
     * @param in the input stream
     *
     * @throws ConfigurationException if an error occurs during the load operation
     */
    public static void load(InputStream in) throws ConfigurationException {
        config.load(in);
    }

    public static void clear() {
        config.clear();
    }

    /**
     * Returns a collection with all configuration event listeners that are
     * currently registered at this object.
     *
     * @return a collection with the registered
     * {@code ConfigurationListener}s (this collection is a snapshot
     * of the currently registered listeners; manipulating it has no effect
     * on this event source object)
     */
    public static Collection<ConfigurationListener> getConfigurationListeners() {
        return config.getConfigurationListeners();
    }

    /**
     * Removes all registered error listeners.
     *
     * @since 1.4
     */
    public static void clearErrorListeners() {
        config.clearErrorListeners();
    }

    public static Integer getInteger(String key, Integer defaultValue) {
        return config.getInteger(key, defaultValue);
    }

    public static boolean isAutoSave() {
        return config.isAutoSave();
    }

    /**
     * Set the comment header.
     *
     * @param header the header to use
     * @since 1.1
     */
    public static void setHeader(String header) {
        config.setHeader(header);
    }

    public static BigDecimal getBigDecimal(String key, BigDecimal defaultValue) {
        return config.getBigDecimal(key, defaultValue);
    }

    /**
     * Get a list of properties associated with the given configuration key.
     *
     * @param key The configuration key.
     * @param defaults Any default values for the returned
     * {@code Properties} object. Ignored if {@code null}.
     *
     * @return The associated properties if key is found.
     *
     * @throws ConversionException is thrown if the key maps to an object that
     * is not a String/List of Strings.
     *
     * @throws IllegalArgumentException if one of the tokens is malformed (does
     * not contain an equals sign).
     */
    public static Properties getProperties(String key, Properties defaults) {
        return config.getProperties(key, defaults);
    }

    public static short getShort(String key, short defaultValue) {
        return config.getShort(key, defaultValue);
    }

    /**
     * Sets the {@code IOFactory} to be used for creating readers and
     * writers when loading or saving this configuration. Using this method a
     * client can customize the reader and writer classes used by the load and
     * save operations. Note that this method must be called before invoking
     * one of the {@code load()} and {@code save()} methods.
     * Especially, if you want to use a custom {@code IOFactory} for
     * changing the {@code PropertiesReader}, you cannot load the
     * configuration data in the constructor.
     *
     * @param ioFactory the new {@code IOFactory} (must not be <b>null</b>)
     * @throws IllegalArgumentException if the {@code IOFactory} is
     *         <b>null</b>
     * @since 1.7
     */
    public static void setIOFactory(PropertiesConfiguration.IOFactory ioFactory) {
        config.setIOFactory(ioFactory);
    }

    /**
     * Locate the specified file and load the configuration. If the configuration is
     * already associated with a source, the current source is not changed.
     * Otherwise (i.e. this is the first load operation), the source URL and
     * the base path are set now based on the source to be loaded.
     *
     * @param fileName the name of the file to be loaded
     * @throws ConfigurationException if an error occurs
     */
    public static void load(String fileName) throws ConfigurationException {
        config.load(fileName);
    }

    public static long getLong(String key) {
        return config.getLong(key);
    }

    /**
     * Returns the associated layout object.
     *
     * @return the associated layout object
     * @since 1.3
     */
    public static PropertiesConfigurationLayout getLayout() {
        return config.getLayout();
    }

    public static short getShort(String key) {
        return config.getShort(key);
    }

    /**
     * Save the configuration to the specified stream, using the specified
     * encoding. If the encoding is null the default encoding is used.
     *
     * @param out the output stream
     * @param encoding the encoding to use
     * @throws ConfigurationException if an error occurs during the save operation
     */
    public static void save(OutputStream out, String encoding) throws ConfigurationException {
        config.save(out, encoding);
    }

    /**
     * {@inheritDoc}
     * @see #getStringArray(String)
     * @param key
     */
    public static List<Object> getList(String key) {
        return config.getList(key);
    }

    /**
     * Controls whether additional files can be loaded by the include = <xxx>
     * statement or not. This is <b>true</b> per default.
     *
     * @param includesAllowed True if Includes are allowed.
     */
    public static void setIncludesAllowed(boolean includesAllowed) {
        config.setIncludesAllowed(includesAllowed);
    }

    /**
     * Determine if this configuration is using delimiters when parsing
     * property values to convert them to lists of values. Defaults to false
     * @return true if delimiters are not being used
     */
    public static boolean isDelimiterParsingDisabled() {
        return config.isDelimiterParsingDisabled();
    }

    /**
     * {@inheritDoc}
     * @see PropertyConverter#toBoolean(Object)
     * @param key
     * @param defaultValue
     */
    public static boolean getBoolean(String key, boolean defaultValue) {
        return config.getBoolean(key, defaultValue);
    }

    /**
     * Load the configuration from the underlying location.
     *
     * @throws ConfigurationException if loading of the configuration fails
     */
    public static void load() throws ConfigurationException {
        config.load();
    }

    public static int getInt(String key, int defaultValue) {
        return config.getInt(key, defaultValue);
    }

    public static String getString(String key, String defaultValue) {
        return config.getString(key, defaultValue);
    }

    /**
     * Appends the content of the specified configuration to this configuration.
     * The values of all properties contained in the specified configuration
     * will be appended to this configuration. So if a property is already
     * present in this configuration, its new value will be a union of the
     * values in both configurations. <em>Note:</em> This method won't work
     * well when appending hierarchical configurations because it is not able to
     * copy information about the properties' structure (i.e. the
     * parent-child-relationships will get lost). So when dealing with
     * hierarchical configuration objects their
     * {@link HierarchicalConfiguration#clone() clone()} methods
     * should be used.
     *
     * @param c the configuration to be appended (can be <b>null</b>, then this
     * operation will have no effect)
     * @since 1.5
     */
    public static void append(Configuration c) {
        config.append(c);
    }

    /**
     * Returns a configuration with the same content as this configuration, but
     * with all variables replaced by their actual values. This method tries to
     * clone the configuration and then perform interpolation on all properties.
     * So property values of the form <code>${var}</code> will be resolved as
     * far as possible (if a variable cannot be resolved, it remains unchanged).
     * This operation is useful if the content of a configuration is to be
     * exported or processed by an external component that does not support
     * variable interpolation.
     *
     * @return a configuration with all variables interpolated
     * @throws ConfigurationRuntimeException if this configuration cannot be
     * cloned
     * @since 1.5
     */
    public static Configuration interpolatedConfiguration() {
        return config.interpolatedConfiguration();
    }

    /**
     * Reloads the associated configuration file. This method first clears the
     * content of this configuration, then the associated configuration file is
     * loaded again. Updates on this configuration which have not yet been saved
     * are lost. Calling this method is like invoking {@code reload()}
     * without checking the reloading strategy.
     *
     * @throws ConfigurationException if an error occurs
     * @since 1.7
     */
    public static void refresh() throws ConfigurationException {
        config.refresh();
    }

    /**
     * Send notification that the configuration has changed.
     */
    public static void configurationChanged() {
        config.configurationChanged();
    }

    /**
     * Save the configuration. Before this method can be called a valid file
     * name must have been set.
     *
     * @throws ConfigurationException if an error occurs or no file name has
     * been set yet
     */
    public static void save() throws ConfigurationException {
        config.save();
    }

    /**
     * Save the configuration to the specified stream.
     *
     * @param writer the output stream used to save the configuration
     * @throws ConfigurationException if an error occurs
     */
    public static void save(Writer writer) throws ConfigurationException {
        config.save(writer);
    }

    public static float getFloat(String key, float defaultValue) {
        return config.getFloat(key, defaultValue);
    }

    /**
     * Return the base path.
     *
     * @return the base path
     * @see FileConfiguration#getBasePath()
     */
    public static String getBasePath() {
        return config.getBasePath();
    }

    /**
     * Returns the footer comment. This is a comment at the very end of the
     * file.
     *
     * @return the footer comment
     * @since 1.10
     */
    public static String getFooter() {
        return config.getFooter();
    }

    public static void setEncoding(String encoding) {
        config.setEncoding(encoding);
    }

    /**
     * Returns the encoding to be used when loading or storing configuration
     * data. This implementation ensures that the default encoding will be used
     * if none has been set explicitly.
     *
     * @return the encoding
     */
    public static String getEncoding() {
        return config.getEncoding();
    }

    /**
     * Returns true if missing values throw Exceptions.
     *
     * @return true if missing values throw Exceptions
     */
    public static boolean isThrowExceptionOnMissing() {
        return config.isThrowExceptionOnMissing();
    }

    public static BigInteger getBigInteger(String key, BigInteger defaultValue) {
        return config.getBigInteger(key, defaultValue);
    }

    public static long getLong(String key, long defaultValue) {
        return config.getLong(key, defaultValue);
    }

    public static Properties getProperties(String key) {
        return config.getProperties(key);
    }

    /**
     * {@inheritDoc}
     * @see #setThrowExceptionOnMissing(boolean)
     * @param key
     */
    public static BigInteger getBigInteger(String key) {
        return config.getBigInteger(key);
    }

    /**
     * Load the configuration from the specified file. If the configuration is
     * already associated with a source, the current source is not changed.
     * Otherwise (i.e. this is the first load operation), the source URL and
     * the base path are set now based on the source to be loaded.
     *
     * @param file the file to load
     * @throws ConfigurationException if an error occurs
     */
    public static void load(File file) throws ConfigurationException {
        config.load(file);
    }

    /**
     * Save the configuration to the specified file. The file is created
     * automatically if it doesn't exist. This doesn't change the source
     * of the configuration, use {@link #setFile} if you need it.
     *
     * @param file the target file
     *
     * @throws ConfigurationException if an error occurs during the save operation
     */
    public static void save(File file) throws ConfigurationException {
        config.save(file);
    }

    /**
     * Retrieve the current delimiter. By default this is a comma (",").
     *
     * @return The delimiter in use
     */
    public static char getDefaultListDelimiter() {
        return AbstractConfiguration.getDefaultListDelimiter();
    }

    public static Long getLong(String key, Long defaultValue) {
        return config.getLong(key, defaultValue);
    }

    /**
     * Return the file where the configuration is stored. If the base path is a
     * URL with a protocol different than &quot;file&quot;, or the configuration
     * file is within a compressed archive, the return value
     * will not point to a valid file object.
     *
     * @return the file where the configuration is stored; this can be <b>null</b>
     */
    public static File getFile() {
        return config.getFile();
    }

    /**
     * Sets the footer comment. If set, this comment is written after all
     * properties at the end of the file.
     *
     * @param footer the footer comment
     * @since 1.10
     */
    public static void setFooter(String footer) {
        config.setFooter(footer);
    }

    /**
     * Obtains the value of the specified key and tries to convert it into a
     * {@code Boolean} object. If the property has no value, the passed
     * in default value will be used.
     *
     * @param key the key of the property
     * @param defaultValue the default value
     * @return the value of this key converted to a {@code Boolean}
     * @throws ConversionException if the value cannot be converted to a
     * {@code Boolean}
     * @see PropertyConverter#toBoolean(Object)
     */
    public static Boolean getBoolean(String key, Boolean defaultValue) {
        return config.getBoolean(key, defaultValue);
    }

    /**
     * Set the name of the file. The passed in file name can contain a
     * relative path.
     * It must be used when referring files with relative paths from classpath.
     * Use {@link AbstractFileConfiguration#setPath(String)
     * setPath()} to set a full qualified file name.
     *
     * @param fileName the name of the file
     */
    public static void setFileName(String fileName) {
        config.setFileName(fileName);
    }

    /**
     * Returns the default list delimiter.
     *
     * @return the default list delimiter
     * @deprecated Use AbstractConfiguration.getDefaultListDelimiter() instead
     */
    @Deprecated
    public static char getDelimiter() {
        return AbstractConfiguration.getDelimiter();
    }

    /**
     * {@inheritDoc} This implementation returns keys that either match the
     * prefix or start with the prefix followed by a dot ('.'). So the call
     * {@code getKeys("db");} will find the keys {@code db},
     * {@code db.user}, or {@code db.password}, but not the key
     * {@code dbdriver}.
     * @param prefix
     */
    public static Iterator<String> getKeys(String prefix) {
        return config.getKeys(prefix);
    }

    /**
     * Sets the property value for including other properties files.
     * By default it is "include".
     *
     * @param inc A String.
     */
    public static void setInclude(String inc) {
        PropertiesConfiguration.setInclude(inc);
    }

    public static Object getReloadLock() {
        return config.getReloadLock();
    }

    /**
     * {@inheritDoc}
     * @see PropertyConverter#toBoolean(Object)
     * @param key
     */
    public static boolean getBoolean(String key) {
        return config.getBoolean(key);
    }

    /**
     * Adds a configuration listener to this object.
     *
     * @param l the listener to add
     */
    public static void addConfigurationListener(ConfigurationListener l) {
        config.addConfigurationListener(l);
    }
}

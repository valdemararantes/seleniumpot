package br.com.galgo.testes.recursos_comuns.utils;

import br.com.galgo.testes.recursos_comuns.pageObject.TelaGalgo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.ITestContext;
import org.testng.TestListenerAdapter;

/**
 * Created by Valdemar on 23/06/2015.
 */
public class TestCaseListener extends TestListenerAdapter {

    private static final Logger log = LoggerFactory.getLogger(TestCaseListener.class);

    @Override
    public void onStart(ITestContext testContext) {
        log.info("\n%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n");
    }

    @Override
    public void onFinish(ITestContext testContext) {
        log.info("\n%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n");
        TelaGalgo.fecharBrowser();
    }
}

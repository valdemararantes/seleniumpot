package br.com.galgo.testes.recursos_comuns.file.entidades;

import br.com.galgo.testes.recursos_comuns.enumerador.config.Ambiente;
import br.com.galgo.testes.recursos_comuns.enumerador.usuario.UsuarioConfig;
import br.com.galgo.testes.recursos_comuns.exception.ErroAplicacao;
import br.com.galgo.testes.recursos_comuns.file.ArquivoUtils;
import org.junit.Assert;

public class Usuario {

    private String login;
    private String senha;
    private Ambiente ambiente;
    private UsuarioConfig usuarioConfig;

    public Usuario(String login, String senha, Ambiente ambiente) {
        this.login = login;
        this.senha = senha;
        this.ambiente = ambiente;
    }

    public Usuario(UsuarioConfig usuarioConfig) {
        this.usuarioConfig = usuarioConfig;
        this.ambiente = usuarioConfig.getAmbiente();
        this.login = usuarioConfig.getLogin();
        try {
            this.senha = ArquivoUtils.getSenha(usuarioConfig);
        } catch (ErroAplicacao e) {
            Assert.fail("Erro ao pegar senha do usuario com login = " + this.login + " - erro = " + e.getMessage());
        }
    }

    public static Usuario toAmbiente(Ambiente amb) {
        Usuario u = new Usuario(null, null, amb);
        return u;
    }

    public Ambiente getAmbiente() {
        if (this.usuarioConfig == null) {
            return ambiente;
        } else {
            return usuarioConfig.getAmbiente();
        }
    }

    public Usuario setAmbiente(Ambiente ambiente) {
        this.ambiente = ambiente;
        return this;
    }

    public String getLogin() {
        return login;
    }

    public Usuario setLogin(String login) {
        this.login = login;
        return this;
    }

    public String getSenha() {
        return senha;
    }

    public Usuario setSenha(String senha) {
        this.senha = senha;
        return this;
    }

    public UsuarioConfig getUsuarioConfig() {
        return usuarioConfig;
    }

    public Usuario setUsuarioConfig(UsuarioConfig usuarioConfig) {
        this.usuarioConfig = usuarioConfig;
        return this;
    }

}

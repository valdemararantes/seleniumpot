package br.com.galgo.testes.recursos_comuns.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.ITestResult;
import org.testng.TestListenerAdapter;

/**
 * Created by valdemar.arantes on 10/12/2015.
 */
public class LogListener extends TestListenerAdapter {

    private static final Logger log = LoggerFactory.getLogger(LogListener.class);

    @Override
    public void onTestStart(ITestResult result) {
        //@formatter:off
        log.info(
            "\n"
            + "*****************************************************************************************************\n"
            + "Executando o teste {}.{}\n"
            + "*****************************************************************************************************\n",
            result.getTestClass().getName(), result.getName());
        //@formatter:on
    }

    @Override
    public void onTestSuccess(ITestResult result) {
        //@formatter:off
        log.info(
            "\n"
            + "*****************************************************************************************************\n"
            + "Teste executado com sucesso em {} seg\n"
            + "*****************************************************************************************************\n",
            Long.valueOf(result.getEndMillis() - result.getStartMillis()).floatValue()/1000);
    }

    @Override
    public void onTestFailure(ITestResult result) {
        //@formatter:off
        log.info(
            "\n"
            + "*****************************************************************************************************\n"
            + "Teste executado com falha em {} seg\n"
            + "*****************************************************************************************************\n",
            Long.valueOf(result.getEndMillis() - result.getStartMillis()).floatValue()/1000);
    }

    @Override
    public void onTestSkipped(ITestResult result) {
        log.info(
            "\n"
                + "*****************************************************************************************************\n"
                + "Teste ignorado\n"
                + "*****************************************************************************************************\n"
        );
    }
}

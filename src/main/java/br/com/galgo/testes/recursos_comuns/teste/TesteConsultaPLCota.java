package br.com.galgo.testes.recursos_comuns.teste;

import br.com.galgo.testes.recursos_comuns.categoria.CategoriaDownload;
import br.com.galgo.testes.recursos_comuns.enumerador.Operacao;
import br.com.galgo.testes.recursos_comuns.enumerador.config.Ambiente;
import br.com.galgo.testes.recursos_comuns.enumerador.menu.SubMenu;
import br.com.galgo.testes.recursos_comuns.enumerador.usuario.Categoria;
import br.com.galgo.testes.recursos_comuns.enumerador.usuario.Papel;
import br.com.galgo.testes.recursos_comuns.enumerador.usuario.UsuarioConfig;
import br.com.galgo.testes.recursos_comuns.exception.ErroAplicacao;
import br.com.galgo.testes.recursos_comuns.file.entidades.Usuario;
import br.com.galgo.testes.recursos_comuns.pageObject.TelaHome;
import br.com.galgo.testes.recursos_comuns.pageObject.TelaLogin;
import br.com.galgo.testes.recursos_comuns.pageObject.consulta.TelaConsultaPLCota;
import br.com.galgo.testes.recursos_comuns.utils.ConstantesTestes;
import br.com.galgo.testes.recursos_comuns.utils.DataUtils;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import java.util.Date;

public class TesteConsultaPLCota extends Teste {

    private final String PASTA_TESTE = "PLCota";

/*
    @Before
    @BeforeMethod
    public void setUp() {
        ambiente = TesteUtils.configurarTeste(Ambiente.PRODUCAO, PASTA_TESTE);
        TelaGalgo.abrirBrowser();
        telaLogin = TelaLogin.irParaTelaLogin();
    }

    @AfterMethod
    public void tearDown() {
        TesteUtils.finalizarTeste(this.getNomeTeste());
    }
*/

    @Test
    @org.testng.annotations.Test
    public void testeConsultaPLCota() throws ErroAplicacao {
        this.setNomeTeste("testeConsultaPLCota.png");
        consultaPLCota();
    }

    @Test
    @org.testng.annotations.Test
    public void testeConsultaPLCotaDetalhes() throws ErroAplicacao {
        this.setNomeTeste("testeConsultaPLCotaDetalhes.png");
        TelaConsultaPLCota telaPLCota = consultaPLCota();
        telaPLCota.consultarDetalhes();
    }

    @Category(CategoriaDownload.class)
    @Test
    @org.testng.annotations.Test
    public void testeDownloadPLCota() {
        this.setNomeTeste("testeDownloadPLCota.png");
        Date dataFiltro = getDataFiltro();
        downloadPLCota(dataFiltro, null);
    }

    private TelaConsultaPLCota consultaPLCota() {
        Date dataFiltro = getDataFiltro();

        final UsuarioConfig usuarioConfig = UsuarioConfig.fromCategoria(ambiente, Categoria.USUARIO_FINAL, Papel
                .AUTO_REGULADOR);

        Usuario usuario = new Usuario(usuarioConfig);
        TelaHome telaHome = TelaLogin.irParaTelaLogin().loginAs(usuario);
        TelaConsultaPLCota telaPLCota = (TelaConsultaPLCota) telaHome.acessarSubMenu(SubMenu.CONSULTA_PL_COTA);
        telaPLCota.consultarFundosAtivosNoMercado(ambiente);
        telaPLCota.preencherFiltrosDataInicial(dataFiltro);
        telaPLCota.clicarBotaoConfirmar();
        telaPLCota.verificarItemEncontrado();
        return telaPLCota;
    }

    private Date getDataFiltro() {
        Date dataFiltro = null;
        if (ambiente == Ambiente.PRODUCAO) {
            dataFiltro = DataUtils.subtrairDias(7, new Date());
        } else {
            dataFiltro = DataUtils.subtrairDias(90, new Date());
        }
        return dataFiltro;
    }

    private void downloadPLCota(Date dataFiltro, Operacao operacao) {
        final UsuarioConfig usuarioConfig = UsuarioConfig.fromCategoria(ambiente, Categoria.USUARIO_FINAL, Papel
                .AUTO_REGULADOR);
        Usuario usuario = new Usuario(usuarioConfig);

        TelaConsultaPLCota telaConsultaPLCota = consultaPLCota();
        telaConsultaPLCota.download(usuario, ConstantesTestes.PATH_DOWNLOAD_PL_COTA, operacao);
    }
}

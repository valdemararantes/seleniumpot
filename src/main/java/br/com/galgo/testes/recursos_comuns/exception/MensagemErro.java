package br.com.galgo.testes.recursos_comuns.exception;

public interface MensagemErro {

    String URL_INACESSIVEL = "URL não carregou.";
    String ARQUIVO_NAO_ENCONTRADO = "Arquivo não encontrado.";
    String ERRO_LOGIN_INVALIDO = "Login inválido ou usuário bloqueado. Favor preencher corretamente "
                                 + "a senha na planilha de senhas do ambiente deste usuário.";
    String EXCEL_ERRO = "Problema com a leitura do excel.";

}

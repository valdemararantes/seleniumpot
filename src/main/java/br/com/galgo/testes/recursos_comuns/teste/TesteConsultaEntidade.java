package br.com.galgo.testes.recursos_comuns.teste;

import br.com.galgo.testes.recursos_comuns.enumerador.Entidade;
import br.com.galgo.testes.recursos_comuns.enumerador.config.Ambiente;
import br.com.galgo.testes.recursos_comuns.enumerador.menu.SubMenu;
import br.com.galgo.testes.recursos_comuns.enumerador.usuario.Categoria;
import br.com.galgo.testes.recursos_comuns.enumerador.usuario.Papel;
import br.com.galgo.testes.recursos_comuns.enumerador.usuario.UsuarioConfig;
import br.com.galgo.testes.recursos_comuns.exception.ErroAplicacao;
import br.com.galgo.testes.recursos_comuns.file.entidades.Usuario;
import br.com.galgo.testes.recursos_comuns.pageObject.TelaHome;
import br.com.galgo.testes.recursos_comuns.pageObject.TelaLogin;
import br.com.galgo.testes.recursos_comuns.pageObject.consulta.TelaConsultaEntidade;
import br.com.galgo.testes.recursos_comuns.utils.Config;
import org.apache.commons.configuration.ConfigurationException;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.concurrent.CompletableFuture;

public class TesteConsultaEntidade extends Teste {

    private static final Logger log = LoggerFactory.getLogger(TesteConsultaEntidade.class);
    private static boolean isLocalDebug;
    private final String PASTA_TESTE = "Log";

    @BeforeClass
    public static void setUpClass() throws ConfigurationException {
        isLocalDebug = Config.getBoolean("app.is_local_debug.enabled", false);
    }

    @Test
    public void testeConsultaEntidadeAutoreguladora(ITestContext testContext) throws ErroAplicacao {
        log.debug("[TesteConsultaEntidade.testeConsultaEntidadeAutoreguladora:Início do método]");

        this.setNomeTeste("testeConsultaEntidadeAutoreguladora.png");

        Usuario usuario = null;
        CompletableFuture<Usuario> futureUsuario = null;
        if (isLocalDebug) {
            usuario = Usuario.toAmbiente(Ambiente.PRODUCAO).setLogin("ELISANGELAPEDRON.SISTEMAGALGO").setSenha(
                    "Galgo999");
            log.debug("Aguardando a conclusão da thread que abre o browser");
            try {
                futureBrowserOpened.get();
            } catch (Exception e) {
                log.error(null, e);
                Assert.fail("Erro ao abrir o browser: " + e.getMessage());
            }
        } else {
            futureUsuario = CompletableFuture.supplyAsync(() -> {
                final UsuarioConfig usuarioConfig = UsuarioConfig.fromCategoria(ambiente, Categoria.USUARIO_FINAL,
                        Papel.ADM_SISTEMA);
                return new Usuario(usuarioConfig);
            });
            try {
                log.debug("Recuperando o usuário e a senha");
                usuario = futureUsuario.get();
            } catch (Exception e) {
                log.error(null, e);
                Assert.fail("Erro ao abrir o browser: " + e.getMessage());
            }
        }

        TelaHome telaHome = TelaLogin.irParaTelaLogin().loginAs(usuario);
        log.debug("Clicando no menu de Cadastro de Entidades");
        TelaConsultaEntidade telaEntidade = (TelaConsultaEntidade) telaHome.acessarSubMenu(SubMenu.CADASTRO_ENTIDADES);
        log.debug("Consultando as entidades");
        WebElement elementToPrint = telaEntidade.consultarEntidadePJ(Entidade.fromPapel(ambiente,
                Papel.AUTO_REGULADOR));
        testContext.setAttribute("elementToPrint", elementToPrint);
        log.debug("Fim do método");
    }
}
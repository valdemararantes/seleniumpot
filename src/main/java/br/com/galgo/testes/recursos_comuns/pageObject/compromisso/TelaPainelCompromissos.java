package br.com.galgo.testes.recursos_comuns.pageObject.compromisso;

import java.io.IOException;
import java.text.ParseException;
import java.util.Date;

import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.galgo.testes.recursos_comuns.enumerador.Servico;
import br.com.galgo.testes.recursos_comuns.enumerador.data.FormatoData;
import br.com.galgo.testes.recursos_comuns.enumerador.filtro.camposTela.CampoFiltroFundosOuCarteiras;
import br.com.galgo.testes.recursos_comuns.file.entidades.Usuario;
import br.com.galgo.testes.recursos_comuns.pageObject.TelaGalgo;
import br.com.galgo.testes.recursos_comuns.pageObject.consulta.TelaFundosOuCarteiras;
import br.com.galgo.testes.recursos_comuns.pageObject.envio.TelaEnvio;
import br.com.galgo.testes.recursos_comuns.pageObject.envio.TelaEnvioExtrato;
import br.com.galgo.testes.recursos_comuns.pageObject.envio.TelaEnvioInfoAnbimaPortal;
import br.com.galgo.testes.recursos_comuns.pageObject.envio.TelaEnvioPlCota;
import br.com.galgo.testes.recursos_comuns.pageObject.envio.TelaEnvioPosicaoArquivo;

public class TelaPainelCompromissos extends TelaGalgo implements TelaEnvio {

    public static final int DEFAULT_VALOR_OCORRENCIA = 2;
    private static Logger log = LoggerFactory.getLogger(TelaPainelCompromissos.class);

    public boolean abriuPainelCompromissos() {
        try {
            return verificaCampoVisivelPorXPath(getXPathBarraDeProgressoVencidosDiasAnteriores());
        } catch (Exception e) {
            return false;
        }
    }

    public int pegarQtdDeEventosPendentes() {
        String valor = ""; 
        valor = getTextoPorXPath(getXPathBarraEventosPendentes());
        
        int inicio = valor.indexOf("(");
        int fim = valor.indexOf(")");;

        final String qtd = valor.substring(inicio + 1, fim).trim().replace(".", "");

        return Integer.valueOf(qtd);
    }

    private String getXPathBarraEventosPendentes() {
        String xPathBarraEventosPendentes = "/html/body/div[2]/table/tbody/tr[2]/td[2]/table/tbody/tr[3]/td/table/tbody/tr/td[2]/div/table/tbody/tr/td/table/tbody/tr/td/div/form/div/div/div[12]/div[1]/a/p";
        
        TelaGalgo.esperarTelaCarregar(xPathBarraEventosPendentes, TelaGalgoEnum.XPATH);
        
        return "/html/body/div[2]/table/tbody/tr[2]/td[2]/table/tbody/tr[3]/td/table/tbody/tr/td[2]/div/table/tbody/tr/td/table/tbody/tr/td/div/form/div/div/div[12]/div[1]/a/p";
    }

    public int getQtdEventosASeremCumpridos() {
        String xPathQtdEventosASeremCumpridos = "/html/body/div[2]/table/tbody/tr[2]/td[2]/table/tbody/tr[3]/td/table/tbody/tr/td[2]/div/table/tbody/tr/td/table/tbody/tr/td/div/form/div/div[5]/table[2]/tbody/tr/td"; 
        
        String valor = "";
        
        TelaGalgo.esperarTelaCarregar(xPathQtdEventosASeremCumpridos, TelaGalgoEnum.XPATH);
        valor = getTextoPorXPath(xPathQtdEventosASeremCumpridos);
        
        int fim = valor.indexOf("itens");
        if (fim == -1) {
            fim = valor.indexOf("item");
        }

        String qtd = valor.substring(0, fim).trim();
        return Integer.parseInt(qtd);
    }

    public void preecherDataFinal(Date data) {
        preencheCampoPorId("fim", FormatoData.DD_MM_YYYY.formata(data));
    }

    public void preecherDataInicial(Date data) {
        preencheCampoPorId("inicio", FormatoData.DD_MM_YYYY.formata(data));
    }

    public void preecherServico(Servico servico) {
        preencherCampoSelect(servico.getValue(), getNameSelectServico());
        esperarTelaCarregar();//TODO ecalderini: nao eh necessario
    }

    public void clicarVisaoConsolidada() {
        clicarItemPorId(getIdVisaoConsolidada());
        esperarTelaCarregar();//TODO ecalderini: nao eh necessario
    }

    public void clicarVisaoNaoConsolidada() {
        clicarItemPorId(getIdVisaoNaoConsolidada());
        esperarTelaCarregar();//TODO ecalderini: nao eh necessario
    }

    private String getIdVisaoConsolidada() {
        return "visaoConsolidadaSim";
    }

    private String getIdVisaoNaoConsolidada() {
        return "visaoConsolidadaNao";
    }

    public void clicarFiltrosDeBusca() {
        TelaGalgo.esperarTelaCarregar(getXLinkBotaoMostrarFiltros(), TelaGalgoEnum.LINK);
        
        clicarItemPorLink(getXLinkBotaoMostrarFiltros());
        TelaGalgo.esperarTelaCarregar();//XXX ecalderini: nao eh necessario
    }

    public boolean clicarVencidosDiasAnteriores() {
        if (pegarQtdDeEventosPendentes() > 0) { // XXX ecalderini: MUITO estranho, ele retorna true mesmo sendo zero em alguns casos.
            clicarBarraEventosAnteriores();
            TelaGalgo.esperarSegundos(5);
            return true;
        }

        return false;
    }

    private void clicarBarraEventosAnteriores() { // pbar-yellow
        TelaGalgo.esperarTelaCarregar(getXPathBotaoBarraDeProgressoVencidosDiasAnteriores(), TelaGalgoEnum.XPATH);
        
        String barraAmarela = "pbar-yellow";
        
        for(int i = 0; i < MAX_RETRY; i++)
        {
            clicarItemPorClassName(barraAmarela);
            TelaGalgo.esperarSegundos(5);
            
            try {
                WebElement webElement = encontrarCampoPorClassName(barraAmarela);
                log.info("[NOK] Nao foi possivel clicar em {} tentativa {} de {}",barraAmarela, i, MAX_RETRY);
                if(webElement != null)
                {
                    continue;
                }
            } catch (Exception e) {
                log.info("[OK] Evento de clique em {} realizado com sucesso na tentativa {} de {}",barraAmarela, i, MAX_RETRY);
                break;
            }
        }
        
        //clicarItemPorXPath(getXPathBotaoBarraDeProgressoVencidosDiasAnteriores()); // XXX ecalderini: ele clica mas o evento nao eh disparado em alguns casos
    }

    public void clicarGrafico() {
        
        TelaGalgo.esperarTelaCarregar(getXPathGrafico(), TelaGalgoEnum.XPATH);
        clicarItemPorXPath(getXPathGrafico());
    }

    public TelaEnvio clicarBotaoEnviar(Servico servico, Usuario usuario, String codigoSTI, String dataBase,
            String cotista, int posicao) throws IOException {
        if (Servico.EXTRATO == servico) {
            return enviarExtrato(codigoSTI, dataBase, cotista, posicao);
        } else if (Servico.PL_COTA == servico) {
            return enviarPlCotaPortal(codigoSTI, dataBase, posicao);
        } else if (Servico.INFO_ANBIMA == servico) {
            return enviarInfoAmbina(codigoSTI, dataBase, posicao);
        } else {
            return enviarPosicaoAtivos(usuario, codigoSTI, dataBase, posicao);
        }
    }

    private TelaEnvio enviarPlCotaPortal(String codigoSTI, String dataBase, int posicao) {
        clicarBotaoEnviar(posicao);
        esperarTelaCarregar();
        return new TelaEnvioPlCota(codigoSTI, dataBase);
    }

    private TelaEnvioPosicaoArquivo enviarPosicaoAtivos(Usuario usuario, String codigoSTI, String dataBase, int posicao) {
        clicarBotaoEnviar(posicao);
        esperarTelaCarregar();
        return new TelaEnvioPosicaoArquivo(usuario, dataBase, codigoSTI);
    }

    public String getDataBase(int posicao) {
        String xPath = "/html/body/div[2]/table/tbody/tr[2]/td[2]/table/tbody/tr[3]/td/table/tbody/tr/td[2]/div/table/tbody/tr/td/table/tbody/tr/td/div/form/div/div[5]/table[1]/tbody/tr["
                + posicao + "]/td[2]";
        
        String dataBase = ""; 
                
        TelaGalgo.esperarTelaCarregar(xPath, TelaGalgoEnum.XPATH);
        dataBase = encontrarCampoPorXPath(xPath).getText();
        return dataBase;
    }

    public String getCodSTI() {
        TelaCompromissosDetalhes telaCompromissosDetalhes = clicarCompromisso();
        String nomeFundo = telaCompromissosDetalhes.getNomeFundo();
        TelaPainelCompromissos telaPainelCompromissos = telaCompromissosDetalhes.clicarVoltar();
        TelaFundosOuCarteiras telaFundosOuCarteiras = telaPainelCompromissos.clicarBuscaFundo();
        telaFundosOuCarteiras.incluirFiltro(CampoFiltroFundosOuCarteiras.RAZAO_SOCIAL, nomeFundo);
        telaFundosOuCarteiras.clicarBotaoConfirmar();
        String codigoSTI = telaFundosOuCarteiras.getCodSTI().trim().replace("-", "");
        telaFundosOuCarteiras.clicarCancelar();
        clicarBotaoConfirmar();
        return codigoSTI;
    }

    public String getCodSTICotista() {
        TelaCompromissosDetalhes telaCompromissosDetalhes = clicarCompromisso();
        String nomeFundo = telaCompromissosDetalhes.getFundoCotista();
        TelaPainelCompromissos telaPainelCompromissos = telaCompromissosDetalhes.clicarVoltar();
        TelaFundosOuCarteiras telaFundosOuCarteiras = telaPainelCompromissos.clicarBuscaFundo();
        telaFundosOuCarteiras.incluirFiltro(CampoFiltroFundosOuCarteiras.RAZAO_SOCIAL, nomeFundo);
        telaFundosOuCarteiras.clicarBotaoConfirmar();
        String codigoSTICotista = telaFundosOuCarteiras.getCodSTI().trim().replace("-", "");
        telaFundosOuCarteiras.clicarCancelar();
        clicarBotaoConfirmar();
        return codigoSTICotista;
    }

    public void incluirFiltroFundo(String codigoSTI) {
        TelaFundosOuCarteiras telaFundosOuCarteiras = clicarBuscaFundo();
        telaFundosOuCarteiras.incluirFiltro(CampoFiltroFundosOuCarteiras.COD_STI, codigoSTI);
        telaFundosOuCarteiras.clicarBotaoConfirmar();
        telaFundosOuCarteiras.escolherPrimeiroItem();
        clicarBotaoConfirmar();
    }

    private TelaEnvio enviarInfoAmbina(String codigoSTI, String dataBase, int posicao) {
        clicarBotaoEnviar(posicao);
        return new TelaEnvioInfoAnbimaPortal(codigoSTI, dataBase);
    }

    private TelaEnvio enviarExtrato(String codigoSTI, String dataBase, String cotista, int posicao) {
        clicarBotaoEnviar(posicao);
        return new TelaEnvioExtrato(dataBase, codigoSTI, cotista);
    }

    private TelaFundosOuCarteiras clicarBuscaFundo() {
        clicarItemPorXPath("/html/body/div[2]/table/tbody/tr[2]/td[2]/table/tbody/tr[3]/td/table/tbody/tr/td[2]/div/table/tbody/tr/td/table/tbody/tr/td/div/form/div/div[4]/div[7]/p/img[2]");
        esperarTelaCarregar();
        
        return new TelaFundosOuCarteiras();
    }

    private TelaCompromissosDetalhes clicarCompromisso() {
        try {
            String xpath1 = "/html/body/div[2]/table/tbody/tr[2]/td[2]/table/tbody/tr[3]/td/table/tbody/tr/td[2]/div/table/tbody/tr/td/table/tbody/tr/td/div/form/div/div[5]/table[1]/tbody/tr[1]/td[5]/img[3]";
            TelaGalgo.esperarTelaCarregar(xpath1, TelaGalgoEnum.XPATH);
            clicarItemPorXPath(xpath1);
            esperarTelaCarregar();
        } catch (Exception e) {
            String xpath2 = "/html/body/div[2]/table/tbody/tr[2]/td[2]/table/tbody/tr[3]/td/table/tbody/tr/td[2]/div/table/tbody/tr/td/table/tbody/tr/td/div/form/div/div[5]/table[1]/tbody/tr[1]/td[6]/img[3]";
            TelaGalgo.esperarTelaCarregar(xpath2, TelaGalgoEnum.XPATH);
            clicarItemPorXPath(xpath2);
            esperarTelaCarregar();
        }
        
        return new TelaCompromissosDetalhes();
    }

    private void clicarBotaoEnviar(int posicao) {
        try {
            
            clicarItemPorXPath("/html/body/div[2]/table/tbody/tr[2]/td[2]/table/tbody/tr[3]/td/table/tbody/tr/td[2]/div/table/tbody/tr/td/table/tbody/tr/td/div/form/div/div[5]/table[1]/tbody/tr["
                    + posicao + "]/td[6]/img[1]");
            TelaGalgo.esperarTelaCarregar();
            
        } catch (Exception e) {
            clicarItemPorXPath("/html/body/div[2]/table/tbody/tr[2]/td[2]/table/tbody/tr[3]/td/table/tbody/tr/td[2]/div/table/tbody/tr/td/table/tbody/tr/td/div/form/div/div[5]/table[1]/tbody/tr["
                    + posicao + "]/td[5]/img[1]");
            TelaGalgo.esperarTelaCarregar();
        }

    }

    private String getXPathGrafico() {
        return "/html/body/div[2]/table/tbody/tr[2]/td[2]/table/tbody/tr[3]/td/table/tbody/tr/td[2]/div/table/tbody/tr/td/table/tbody/tr/td/div/form[2]/div/div[3]/div[4]/div[2]/div/map/area[1]";
    }

    private String getXPathBotaoBarraDeProgressoVencidosDiasAnteriores() {
        return "//div[@id='anbima-content-main']/div/div[12]/div/a/p";
    }

    private String getXPathBarraDeProgressoVencidosDiasAnteriores() {
        return "//div[@class='ui-progressbar-value ui-widget-header ui-corner-left']";
    }

    private String getNameSelectServico() {
        return "servico";
    }

    private String getXLinkBotaoMostrarFiltros() {
        return "clique aqui para mostrar os filtros de busca";
    }

    
    @Override
    @Deprecated
    public void enviar() throws ParseException {
        
    }

}

package br.com.galgo.testes.recursos_comuns.pageObject;

import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.testng.annotations.Test;

/**
 * Created by valdemar.arantes on 14/01/2016.
 */
public class TelaGalgoTest {

    @Test
    public void testAbrirUrl() throws Exception {
        FirefoxProfile profile = new FirefoxProfile();
        FirefoxDriver driver = new FirefoxDriver(profile);
        driver.get("http://portal.producao.sistemagalgo");
        Thread.sleep(3000);
        driver.get("http://portal.homologacao.sistemagalgo");
    }

}